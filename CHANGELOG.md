# v1.0.4
## Embeddings API: added threads (number of gunicorn threads) as configurable parameter in docker call 

- threads: default: 2; the more threads are used, the higher the memory consumption. 

# v1.0.3
## Embeddings API: using semaphore to prevent queuing; added configurable timeout
- ENTRYPOINT in Dockerfile now works with gunicorn
- Timeout can be set in docker run command and will be passed to gunicorn; example: `sudo docker run -p 4995:4995 api -t 40`
- Used gevent semaphore to prevent queuing; if more than one gunicorn thread is used, a 503 error ('The service is busy and queueing is not possible.') will be thrown if a request is made while the API is busy processing another request.


# v1.0.2
## Recommendation Engine: fixed Postgresql connection problem
- Fix for https://europeana.atlassian.net/browse/EA-3102

# v1.0.1
## Embeddings API: replaced gevent to be default WSGI server
- europeana_embeddings_api.py changed to gevent as default
- added optional webserver cmd parameter in europeana_embeddings_api.py
- Added gevent to requirements.txt
- europeana_embeddings_api.py - added model reset
