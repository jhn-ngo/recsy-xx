#
# Copyright (c) 2021 Pangeanic/JHN.
#
# This file is part of RecSy (EuropeanaXX).
# See https://bitbucket.org/jhn-ngo/recsy-xx for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from collections import namedtuple
from laserembeddings import Laser
from scipy.spatial import distance as sc_distance

from haystack.database.base import Document


class LaserRanker:
    """
    Laser ranker for reranking documents using the laser embeddings:
    (https://github.com/yannvgn/laserembeddings)

    With the ranker, you can:
     - directly get new scores of the documents via rerank()
    """

    def __init__(self,):
        """ Initialize laser embedder. """

        self.model = Laser()

    def predict(self, query: str, documents: [Document], top_k: int = 5):
        """
        Use Laser embedder to find top k most relevant passages to return in the supplied list of Document.

        Returns dictionaty containing initial query,

        :param query: query string
        :param documents: list of Document in which to search for the answer
        :param top_k: the maximum number of answers to return
        :return: dict containing query and top k documents

        """

        ranked_documents = []
        ranked_document = namedtuple("document", ["id", "text", "score"])
        corpus = [doc.text for doc in documents]
        # TODO: in case we have already encoded vectors there is no need to encode corpus once again
        corpus_embeddings = self.model.embed_sentences(corpus, lang="en")
        query_embedding = self.model.embed_sentences([query], lang="en")
        distances = sc_distance.cdist(query_embedding, corpus_embeddings, "cosine")[0]
        closests = zip(
            [doc.id for doc in documents], [doc.text for doc in documents], distances
        )
        closests = sorted(
            closests, key=lambda x: x[2]
        )  # sorting by distance: less distance - better relevance

        for (doc_id, doc_text, ranker_distance,) in closests[:top_k]:
            ranked_documents.append(
                ranked_document(id=doc_id, text=doc_text, score=(1 - ranker_distance))
            )  # higher score - better relevance

        results = {"query": query, "top_k": top_k, "ranked_documents": ranked_documents}

        return results

