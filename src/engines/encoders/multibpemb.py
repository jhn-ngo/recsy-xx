#
# Copyright (c) 2021 Pangeanic/JHN.
#
# This file is part of RecSy (EuropeanaXX).
# See https://bitbucket.org/jhn-ngo/recsy-xx for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from bpemb import BPEmb

multibpemb = BPEmb(lang="multi", vs=1000000, dim=300)

text = 'John F. Kennedy said "Ich bin ein Pfannkuchen". 这是一个中文句子.日本語の文章です。'

subwords = multibpemb.encode(text)

print(" ".join(subwords))
