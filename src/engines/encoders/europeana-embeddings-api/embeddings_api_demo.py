#
# Copyright (c) 2021 Anacode GmbH.
#
# This file is part of Europeana XX.
# See https://pro.europeana.eu/project/europeana-xx for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from pprint import pprint

import requests

RECORDS = [{"id": "00101/40C346868A55C2D299F3E3A5387DFB5D1765292G",
            "title": ["Pal cio Jamai, Meknes : exemplares de azulejos rabes"],
            "description": ["Bibliografia", "Bibliographic item"],
            "creator": [" S i m e s , J. M. dos Santos, 1907-1972"], "tags": [" P a l c i o Jamai (Meknes,Marrocos)",
                                                                              "Azulejos, Marrocos",
                                                                              "738.8(64)(084.121)"],
            "places": ["Marrocos"],
            "times": []},
           {"id": "00101/40C346868A55C2D299F3E3A5387DFB5D1765292F",
            "title": ["Pal cio Jamai, Meknes : exemplares de azulejos rabes"],
            "description": ["Monografia", "Monographic bibliographic item"],
            "creator": [" S i m e s , J.M. dos Santos, 1907-1972"],
            "tags": [" P a l c i o Jamai (Meknes, Marrocos)", "Azulejos, Marrocos",
                     "738.8(64)(084.121)"],
            "places": ["Marrocos"],
            "times": []}]

response = requests.post("http://85.214.245.200:4995/embedding_api/embeddings",
                         json={"records": RECORDS})
pprint(response.json())
print(len(response.json()["data"][0]["embedding"]))
