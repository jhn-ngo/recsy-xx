#### Instructions for deploying Europeana embeddings API

##### General

In this folder, you will find:

- **europeana-embeddings-docker/**: This is the docker image that contains the API script as well as the embedding model
. The
 docker
 image is built on the base image for python 3.6.
- **europeana_embedding_api_docs_v2.pdf**: documentation of API usage
- **embeddings_api_demo.py**: python3 script demonstrating the API usage

##### Deployment of the API docker image

After building the image, please run the container as follows:

`docker run -p 4995:4995 <image name>`

The API will be running on port 4995. 

There are two optional gunicorn parameters to adjust the deployment:

- timeout (default: 30): in seconds; specifies the overall timeout 
- threads (default: 2): the memory consumption increases with the number of gunicorn threads. To minimize memory consumption, please set threads to 1. If threads is bigger than 1, a 503 error will be thrown for request queueing attempts.  

Example with timeout of 60 sec and 1 thread:

`docker run -p 4995:4995 <image name> --timeout 60 --threads 1`