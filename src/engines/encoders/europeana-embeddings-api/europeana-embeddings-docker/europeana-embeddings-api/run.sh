#!/bin/bash
gunicorn -b 0.0.0.0:4995 --pythonpath europeana-embeddings-api/ europeana_embeddings_api:app --threads 2 "$@"
