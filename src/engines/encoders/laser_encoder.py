#
# Copyright (c) 2021 Pangeanic/JHN.
#
# This file is part of RecSy (EuropeanaXX).
# See https://bitbucket.org/jhn-ngo/recsy-xx for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
""" Sentence encoder script

This script allows to encode sentences with help of LaserEmbeddings:
https://github.com/yannvgn/laserembeddings
This tool accepts full entities filename and suffix, which set up naming of created files.
It generates two files: .bin file containing embeddings and .txt file containing keys

This script requires that `numpy`, `tqdm` and `laserembeddings` be installed within the Python
environment you are running this script in.

This file cannot be imported as a module.
"""

import os
import csv
import numpy
import argparse
import tqdm
from pathlib import Path
from laserembeddings import Laser


def set_parser() -> argparse.ArgumentParser:
    """Setup parser needed for the script. No any parameters required.

    Returns
    -------
    parser
        an ArgumentParser object, from which entities filename and suffix could be extracted
    """

    parser = argparse.ArgumentParser(description="Parse encoder params: ")
    parser.add_argument(
        "-e",
        "--entspath",
        dest="entspath",
        default="../data/parsed_entities/",
        help="path to the directory with entities",
    )
    parser.add_argument(
        "-s",
        "--savepath",
        dest="savepath",
        default="../data/encoded_data/",
        help="path to directory to which encoded files will be saved",
    )

    return parser


def ner_prefix(use_ner: bool):
    if use_ner:
        using_ner = "with_ners"
    else:
        using_ner = "no_ners"

    return using_ner


def encode_entities(laser, in_fname, out_suffix, savepath, collection_name):
    # Setup variables
    BATCH = 50
    text_list = []
    narrays = []
    nkeys = []
    embed_fn = os.path.join(savepath, f"{collection_name}_embeddings_{out_suffix}.bin")
    keys_fn = os.path.join(savepath, f"{collection_name}_keys_{out_suffix}.txt")
    # if file is already exisits in target directory do no overwrite it
    if Path(embed_fn).is_file():
        print(f"Collection {collection_name} will be skipped, cause it's already downloaded in {savepath}")
        pass
    else:
        print(f"Collection {collection_name} will be processed")
        fembed = open(
            embed_fn, "wb"
        )
        fkeys = open(
            keys_fn, "w"
        )

        with open(in_fname, "r") as f:
            fcsv = csv.reader(f)
            for row in tqdm.tqdm(fcsv):
                key, entities = row[0], ",".join(row[1:])
                text_list.append(entities)
                nkeys.append(key)
                if len(text_list) >= BATCH:
                    narrays = [
                        i for i in laser.embed_sentences(text_list, lang="en").tolist()
                    ]
                    for narray in narrays:
                        numpy.save(fembed, narray)
                    for key in nkeys:
                        fkeys.write(key + "\n")
                    text_list = []
                    nkeys = []

            if text_list:
                narrays += [i for i in laser.embed_sentences(text_list, lang="en").tolist()]
                for narray in narrays:
                    numpy.save(fembed, narray)
                for key in nkeys:
                    fkeys.write(key + "\n")


if __name__ == "__main__":
    # Parse params from CLI:
    parser = set_parser()
    args = parser.parse_args()
    entspath = args.entspath
    savepath = args.savepath
    entities_data_dir = os.fsencode(entspath)

    # Initializing LASER
    print("Initializing Laser ... \n")
    laser = Laser(embedding_options={"max_sentences": 1})

    # browse folder
    for file in os.listdir(entities_data_dir):
        fn = os.fsdecode(file)
        full_fn_path = os.path.join(entspath, fn)
        # encoding each entity
        if "exhibits" in fn:
            print("--- Encoding exhibits ---")
            if "with_ners" in fn:
                encode_entities(laser, full_fn_path, "with_ners", savepath, "exhibits")
            elif "no_ners" in fn:
                encode_entities(laser, full_fn_path, "no_ners", savepath, "exhibits")
        elif "movies" in fn:
            print("--- Encoding movies ---")
            if "with_ners" in fn:
                encode_entities(laser, full_fn_path, "with_ners", savepath, "movies")
            elif "no_ners" in fn:
                encode_entities(laser, full_fn_path, "no_ners", savepath, "movies")
        elif "places" in fn:
            print("--- Encoding places ---")
            if "with_ners" in fn:
                encode_entities(laser, full_fn_path, "with_ners", savepath, "places")
            elif "no_ners" in fn:
                encode_entities(laser, full_fn_path, "no_ners", savepath, "places")
        elif "personalities" in fn:
            print("--- Encoding personalities ---")
            if "with_ners" in fn:
                encode_entities(
                    laser, full_fn_path, "with_ners", savepath, "personalities"
                )
            elif "no_ners" in fn:
                encode_entities(
                    laser, full_fn_path, "no_ners", savepath, "personalities"
                )
        elif "gen_tree_individuals" in fn:
            print("--- Encoding gen_tree_individuals ---")
            if "with_ners" in fn:
                continue
                # encode_entities(
                #     laser, full_fn_path, "with_ners", savepath, "gen_tree_individuals"
                # )
            elif "no_ners" in fn:
                encode_entities(
                    laser, full_fn_path, "no_ners", savepath, "gen_tree_individuals"
                )
        elif "family_names" in fn:
            print("--- Encoding family_names ---")
            if "with_ners" in fn:
                encode_entities(
                    laser, full_fn_path, "with_ners", savepath, "family_names"
                )
            elif "no_ners" in fn:
                encode_entities(
                    laser, full_fn_path, "no_ners", savepath, "family_names"
                )
        else:
            continue


