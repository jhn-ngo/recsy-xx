from bonobo.config import Configurable, Service, Option
import json
import re
import iso639
from jsonbender import bend, K, S, F, OptionalS
from jsonbender.list_ops import ForallBend, FlatForall
from jsonbender.control_flow import Alternation
import argparse
import flatten_dict
from collections import defaultdict
import hashlib
import unidecode
from pprint import pprint


def FProxies(proxies):
    for p in proxies:
        if not p["europeanaProxy"]:
            #pprint(p)
            return p
    return None

def FAggregations(aggs):
    # TODO: what to do if there more than on aggregation?
    return aggs[0]


def FWebResources(wr):
    # TODO: what to do if there more than on aggregation?
    return wr[0]

def FID(euID, otype):
    if isinstance(euID, (list,tuple)): euID = euID[0]
    if euID: euID = euID.strip()
    return "{}:{}:{}".format(otype, EuropeanaTransform.PROVIDER_ID, euID)

# TODO: replace with class but make it fast (currently it is very slow)
# class FID(dict):
#     DELIMITER = ":"
#     def __init__(self, euID):
#         dict.__init__(self, object_id=euID)
#         self.object_id = euID
#     def __str__(self):
#         return "{}{}{}".format(EuropeanaTransform.PROVIDER_ID, self.DELIMITER, self.object_id)
#
#     def __repr__(self):
#         return self.__str__()
#
#     def __hash__(self):
#         return hash(self.__str__().encode())
#
#     @staticmethod
#     def is_valid(id: str):
#         provider_id,object_id = id.split(FID.DELIMITER)
#         return provider_id == EuropeanaTransform.PROVIDER_ID and object_id

def FType(type_dict):
    if EuropeanaTransform.TYPE: return EuropeanaTransform.TYPE
    if not type_dict or isinstance(type_dict, (str, bytes)): return "other"
    # TODO: implement multilanguage classifier to classify the heritageObject to one these categories:
    # - book/document/painting/postcard/...
    type_name = type_dict.get("en")
    if not type_name: return "other"
    if isinstance(type_name, (str, bytes)): type_name = [type_name]
    if type_name[0] in ["painting", "frame"]: return "painting"
    return "other"

def FLanguage1(value, langs=["def"]):
    #print("LANGS: {}".format(langs))
    if isinstance(value, (str, bytes)):
        return
    # Pick first language value
    if isinstance(value, dict):
        for k,v in value.items():
            if isinstance(v, dict):
                found = False
                for kk,vv in v.items():
                    if kk in langs:
                        value[k] = "\n".join(vv)
                        found =True
                        break
                if not found: FLanguage(v, langs)
    if isinstance(value, (list,tuple)):
        for v in value:
            FLanguage(v, langs)
#    if isinstance(value, list):
#        nvalue = []
#        for v in value:
#            nvalue.append(FLanguage(v, langs))
#        return nvalue
    return

CACHE=dict()

def FLanguage(object, cache=False):
    if cache and object["providerId"] in CACHE:
        return CACHE[str(object["providerId"])]
    flat_object = flatten_dict.flatten(object)
    langs = []
    new_object = defaultdict(dict)
    for tuple_key, value in flat_object.items():
        pot_lang = tuple_key[-1]
        new_value = "\n".join(value) if isinstance(value, (list, tuple)) else value
        pot_lang = re.split("-", pot_lang)[0] # strip us from en-us (if exists)
        try:
            if not pot_lang: continue
            #print("POT LANG: {}".format(pot_lang) )
            if len(pot_lang) == 3 and pot_lang not in ["def","url"] and not iso639.is_valid639_2(pot_lang):
                continue
            if not iso639.is_valid639_1(pot_lang):
                pot_lang = iso639.to_iso639_1(pot_lang)
            if not pot_lang: continue
            langs.append(pot_lang)
            new_object[("_translations",pot_lang) + tuple_key[:-1]] = new_value
            # TODO: if objectLang and pot_lang == objectLang:
            #     new_object[tuple_key[:-1]] = new_value
        except iso639.NonExistentLanguageError:
            if pot_lang == "def" or pot_lang == "": # TODO:  or objectLang and pot_lang == objectLang
                new_object[tuple_key[:-1]] = new_value
            else:
                new_object[tuple_key] = new_value
        except ValueError:
            new_object[tuple_key] = new_value
    unflat_object = flatten_dict.unflatten(new_object)
    if cache:
        CACHE[str(unflat_object["providerId"])] = unflat_object
    return unflat_object

def FObjectLanguage(lang_dict):
    if "def" in lang_dict: return lang_dict["def"][0]
    return list(lang_dict.values())[0]

def FDataProvider(dataProvider):
    dataProviderObj = FLanguage({"preferredLabel": dataProvider})
    # Create artificial europeana-like ID
    euID = unidecode.unidecode(dataProviderObj.get("preferredLabel", "")).replace(" ", "_")
    fid = FID(euID, "organisation")
    dataProviderObj.update({"providerId": fid})
    return dataProviderObj

class EuropeanaTransform(Configurable):
    PROVIDER_ID = "europeana"
    TYPE = "other"

    MAPPING = {
        "heritageObject": {
            "providerId"                : S("object", "about") >> F(FID, "heritageObject"),
            #"objectLanguage"    : Alternation(S("object", "proxies") >> F(FProxies) >> S("dcLanguage"),
            #                                  S("object", "language")),
            "objectLanguage": S("object", "proxies") >> F(FProxies) >> OptionalS("dcLanguage", default={"def": [None]}) >> F(FObjectLanguage),
            "type"              : S("object", "proxies") >> F(FProxies) >> OptionalS("dcType", default=None) >> F(FType),
            "mediaType"         : S("object", "proxies") >> F(FProxies) >> S( "edmType"),
            "title"             : S("object", "proxies") >> F(FProxies) >> OptionalS("dcTitle", default=None),

            "description"       : S("object", "proxies") >> F(FProxies) >> OptionalS( "dcDescription"),

            "identifier"        : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dcIdentifier"),
            "source"            : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dcSource"),

            "created"           : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dctermsCreated"),
            "issued"            : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dctermsIssued"),

            "format"            : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dcFormat"),
            "relation"          : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dcRelation"),
            "rights"            : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dcRights"),

            "alternative"       : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dctermsAlternative"),
            "extent"            : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dctermsExtent"),
            "medium"            : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dctermsMedium"),
            "provenance"        : OptionalS("object", "proxies") >> F(FProxies) >> OptionalS( "dctermsProvenance"),
        },

        "webLinks": [
            {
             "_dataProvider" : S("object", "aggregations") >> F(FAggregations) >> S("edmDataProvider") >> F(FDataProvider),
             "providerId"    : S("object", "aggregations") >> F(FAggregations) >> OptionalS("edmIsShownBy") >>  F(FID, "webLink"),
             "url"           : S("object", "aggregations") >> F(FAggregations) >> OptionalS("edmIsShownBy"),
             "urlType"       : "media"
             },
            {
             "_dataProvider" : S("object", "aggregations") >> F(FAggregations) >> S("edmDataProvider") >> F(FDataProvider),
             "providerId"    : S("object", "aggregations") >> F(FAggregations) >> OptionalS("edmIsShownAt") >>  F(FID, "webLink"),
             "url"           : S("object", "aggregations") >> F(FAggregations) >> OptionalS("edmIsShownAt"),
             "urlType"       : "webpage"
             },
            {
             "_dataProvider" : S("object", "aggregations") >> F(FAggregations) >> S("edmDataProvider") >> F(FDataProvider),
             "providerId"    : S("object", "aggregations") >> F(FAggregations) >> OptionalS("edmObject") >>  F(FID, "webLink"),
             "url"           : S("object", "aggregations") >> F(FAggregations) >> OptionalS("edmObject"),
             "urlType"       : "preview_source"
             },
            {
             "_dataProvider"  : S("object", "aggregations") >> F(FAggregations) >> S("edmDataProvider") >> F(FDataProvider),
             "providerId"     : S("object", "aggregations") >> F(FAggregations) >> OptionalS("hasView") >>  F(FID, "webLink"),
             "url"            : S("object", "aggregations") >> F(FAggregations) >> OptionalS("hasView"),
             "urlType"        : "other"
            },
            # TODO: Extract additional links from webResources
            #S("object", "aggregations") >> F(FAggregations) >> OptionalS("webResources", default=[])
            #                            >> ForallBend({"link": S("about"),
            #                                           "type": "other"}) # TODO: dataProvider, format, created
        ],

        "agents": OptionalS("object", "agents", default=[]) >> ForallBend({"providerId"                 : S("about")  >> F(FID, "agent"),
                                                               "preferredLabel"     : OptionalS("prefLabel"),
                                                               "alternativeLabel"   : OptionalS("altLabel"),
                                                               "identifier"         : OptionalS("dcIdentifier"),
                                                               "biographicalInformation": OptionalS("rdaGr2BiographicalInformation"),
                                                               "gender"             : OptionalS("rdaGr2Gender"),
                                                               "professionOrOccupation": OptionalS("rdaGr2ProfessionOrOccupation"),

                                                               "dateOfBirth"        : OptionalS("rdaGr2DateOfBirth"),
                                                               "dateOfDeath"        : OptionalS("rdaGr2DateOfDeath"),
                                                               "placeOfBirth"       : OptionalS("rdaGr2PlaceOfBirth"),
                                                               "placeOfDeath"       : OptionalS("rdaGr2PlaceOfDeath"),

                                                               "dateOfEstablishment": OptionalS("rdaGr2DateOfEstablishment"),
                                                               "dateOfTermination"  : OptionalS("rdaGr2DateOfTermination"),

                                                               "sameAs"             : OptionalS("owlSameAs")

                                                               }),
        "places": OptionalS("object", "places", default=[]) >> ForallBend({"providerId"             : OptionalS("about", default="")  >> F(FID, "place"),
                                                               "preferredLabel" : OptionalS("prefLabel"),
                                                               "alternativeLabel": OptionalS("altLabel"),
                                                               "latitude"       : OptionalS("latitude"),
                                                               "longitude"      : OptionalS("longitude"),
                                                               "altitude"       : OptionalS("altitude"),
                                                               "note"           : OptionalS("note")
                                                               }),
        "timeSpans": OptionalS("object", "timespans", default=[]) >> ForallBend({"providerId"             : S("about")  >> F(FID, "timeSpan"),
                                                               "preferredLabel" : OptionalS("prefLabel"),
                                                               "alternativeLabel": OptionalS("altLabel")
                                                               }),

        "concepts": OptionalS("object", "concepts", default=[]) >> ForallBend({"providerId": S("about")  >> F(FID, "concept"),
                                                                                 "preferredLabel": OptionalS("prefLabel"),
                                                                                 "alternativeLabel": OptionalS("altLabel")
                                                                                 }),
        "europeana": OptionalS("object", "europeanaAggregation")
    }


    def __call__(self, row):
        trow = bend(self.MAPPING, row)
        #FLanguage(trow["heritageObject"]["objectLanguage"])
        #print(trow)
        trow["heritageObject"] = FLanguage(trow["heritageObject"])
        if trow["heritageObject"]["objectLanguage"]:
            trow["heritageObject"]["objectLanguage"] =  iso639.to_iso639_1(trow["heritageObject"]["objectLanguage"])
        trow["webLinks"] = [FLanguage(wl) for wl in trow["webLinks"] if not wl["providerId"].endswith(":None")]

        for key in ["agents", "places", "timeSpans", "concepts"]:
            trow[key] = [FLanguage(wl, cache=True) for wl in trow[key]]
            if not trow[key]: del trow[key]
        #pprint(trow)
        yield trow

    def run(self, row):
        l = [r for r in self(row)]
        return l[0]

def FListToStr(in_list):
    if not in_list: return ""
    if isinstance(in_list, (str, bytes)): return in_list
    return ". ".join(in_list)

def FLangAware(lang_aware_value):
    if not lang_aware_value: return []
    anylang = list(lang_aware_value.keys())[0]
    for lang in ['en', 'eng' 'def',anylang ]:
        if lang in lang_aware_value:
            return lang_aware_value[lang]
    return []

def FListLangAwareToStr(lang_aware_value):
    return FListToStr(FLangAware(lang_aware_value))

def FRightsIcon(rights_url):
    rights2icon = {
        'http://creativecommons.org/licenses/by-nc-nd/4.0/': 'https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by-nc-nd.svg',
        'http://rightsstatements.org/vocab/InC/1.0/': 'https://rightsstatements.org/files/buttons/InC.dark-white-interior.svg',
        'http://creativecommons.org/licenses/by-nc-sa/4.0/': 'https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by-nc-sa.svg',
        'http://creativecommons.org/licenses/by-nc/4.0/': 'https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc.svg',
        'http://creativecommons.org/licenses/by/4.0/': 'https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg',
        'http://creativecommons.org/publicdomain/zero/1.0/': 'https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg',
        'http://rightsstatements.org/vocab/InC-EDU/1.0/': 'https://rightsstatements.org/files/buttons/InC-EDU.dark-white-interior.svg'
    }
    return rights2icon.get(rights_url, 'https://rightsstatements.org/files/buttons/UND.dark-white-interior.svg')


class EuropeanaShortTransform(EuropeanaTransform):
    MAPPING = {
        "id": S("id"),
        "title": OptionalS("dcTitleLangAware") >> F(FListLangAwareToStr),
        "description": OptionalS("dcDescriptionLangAware") >> F(FListLangAwareToStr),
        "url": OptionalS("edmIsShownAt", default=[]) >> F(FListToStr),
        "image_url": OptionalS("edmIsShownBy", default=[]) >> F(FListToStr),
        "preview_url": OptionalS("edmPreview", default=[]) >> F(FListToStr),
        "type": S("type"),
        "tags": OptionalS("edmConceptPrefLabelLangAware") >> F(FLangAware),
        "provider": {
            "id": S("dataProvider") >> F(FListToStr),
            "logo": K(None)
        },
        "creator": OptionalS("dcCreator", default=[]) >> F(FListToStr),
        "rights": {
            "url" : S("rights") >> F(FListToStr),
            "icon": S("rights") >> F(FListToStr) >> F(FRightsIcon)
        },
        "location":
            {
                "lat" : OptionalS("edmPlaceLatitude", default=[]),
                "lon" : OptionalS("edmPlaceLongitude", default=[]),
                "label": OptionalS("edmPlaceLabelLangAware", default=[]) >> F(FLangAware)
            }

    }

    def __call__(self, row):
        trow = bend(self.MAPPING, row)
        self._process_location(trow)
        yield trow

    def _process_location(self, trow):
        # Remove empty location
        if not trow["location"]["lat"] and not trow["location"]["lon"]:
            del trow["location"]
        else:
            trow["location"] = [
                {"lat": lat,
                 "lon": lon,
                 "label": label } for lat,lon,label in zip(trow['location']['lat'], trow['location']['lon'], trow['location']['label'])]
        return trow

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i', type=str, default="europeana.json", help="Input JSON file")
    parser.add_argument('--output', '-o', type=str, default="europeana_transform.json", help="Output JSON file")
    parser.add_argument('--type', '-t', type=str, help="Enforce object type", choices=['book', 'painting', 'photograph'])
    return parser.parse_args()

if __name__ == "__main__":
    import bonobo, sys
    args = parse_args()

    OTYPE=args.type
    def get_graph():
        graph = bonobo.Graph()
        et = EuropeanaTransform()
        et.TYPE = args.type
        graph.add_chain(bonobo.JsonReader(path=args.input), et, bonobo.JsonWriter(path=args.output))
        return graph

    bonobo.run(get_graph())
