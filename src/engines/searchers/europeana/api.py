import requests
from engines.searchers.europeana.transform import EuropeanaShortTransform
from pprint import pprint
class EuropeanaAPI:
    def __init__(self, wskey, base_url='https://api.europeana.eu/record'):
        self.base_url = base_url
        self.version = 'v2'
        self.wskey = wskey
        self.transform = EuropeanaShortTransform()

    def search(self, query='asiago', qf=None, rows=100, start=1, theme=None, sort=None, reusability=None) -> dict:
        """
        :param str query: param of query for Europeana API.
        :param int rows: quantity of returns records.
        :return: response from Europeana.
        """
        endpoint_name = 'search.json'
        params = {
            'query': query,
            'rows': rows,
            'start': start,
            'profile': 'standard'
        }
        if qf:
            params['qf'] = qf
        if theme:
            params['theme'] = theme
        if sort:
            params['sort'] = sort
        if reusability:
            params['reusability'] = reusability

        response: dict = self._get_api_call(endpoint_name, params)

        return {'results': self._transform(response.get('items', [])),
                'count': response.get('itemsCount', 0),
                'total_count': response.get('totalResults', 0)
                }

    def search_ids(self, ids: list) -> list:
        """Function form query string to europeana search API"""

        query_string = "(europeana_id:"
        keys_l = len(ids)

        for i in range(keys_l):
            if i == keys_l - 1:  # last element in the list
                query_string += f'"{ids[i]}")'
            else:
                query_string += f'"{ids[i]}" OR europeana_id:'

        return self.search(query=query_string)

    def _get_api_call(self, endpoint_name: str, params: dict) -> dict:
        """
        Base method. Make call for Europeana API.

        :param str endpoint_name: target endpoint for Europeana.
        :param dict params: contains param for request. Like 'query', 'row', etc.
        :return: API response itself.
        """
        params.update({'wskey': self.wskey})
        # params.update({'media': 'true'})
        url = f'{self.base_url}/{self.version}/{endpoint_name}'
        response: dict = requests.get(url, params=params).json()
        return response

    def _transform(self, results: list) -> list:
        tr_results = []
        for item in results:
            record_data = item  #TODO: detailed record data: requests.get(item['link']).json()
            tr_result = self.transform.run(record_data) # self._transform_object(self.transform.run(record_data))
            tr_results.append(tr_result)
        return tr_results

    def _transform_object(self, object):
        tr_object = dict()
        pprint(object)
        for field,value in object["heritageObject"].items():
            if field != "_translations":
                tr_object[field] = value
        if "_translations" in object["heritageObject"] and ('title' not in tr_object or 'description' not  in tr_object):
            # TODO: get language-specific
            value = object["heritageObject"]["_translations"]
            anylang = list(value.keys())[0]
            for lang in [anylang, 'def', 'en', 'eng']:
                if lang in value:
                    tr_object['title'] = value[lang].get('title', tr_object.get('title', 'no title'))
                    tr_object['description'] = value[lang].get('description', tr_object.get('description', '---'))
        # Postprocess - remove "null" values
        for f in ["title", "desciption"]:
            if tr_object.get(f) == "null": tr_object[f] = ""
        tr_object["webLink"] = object["webLinks"]
        tr_object["europeana"] = object.get("europeana")
        return tr_object


if __name__ == "__main__":
    from pprint import pprint
    import sys

    eusearch = EuropeanaAPI(sys.argv[1])
    #pprint(eusearch.search(sys.argv[1], rows=3))
    pprint(eusearch.search(sys.argv[2:]))
