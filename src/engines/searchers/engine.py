import os
import sys
import tqdm
import numpy
import argparse
from milvus import Milvus, IndexType, MetricType, Status


class SearchEngine:
    def __init__(
        self,
        table_name="titles_with_entities",
        host="localhost",
        port="19530",
        dimension=1024,
        index_type=IndexType.IVF_SQ8,
        index_params={"nlist": 16384}
    ):
        self.milvus = Milvus(host=host, port=port)
        self.table_name = table_name
        status, exists = self.milvus.has_collection(table_name)
        if not exists:
            status = self.milvus.create_collection(
                {"collection_name": table_name,
                 "dimension": dimension}
            )
            print(status)
            self.milvus.create_index(table_name, index_type, index_params)
        # Keep partitions data
        status, partitions = self.milvus.list_partitions(collection_name=table_name)
        self.partitions = {p.tag for p in partitions}

    def index(self, vectors, keys, partition=None):
        # First, create partition if doesn't exist
        if partition and not partition in self.partitions:
            print(
                f"Creating parition: {partition} \n"
            )
            self.milvus.create_partition(collection_name=self.table_name, partition_tag=partition)
            self.partitions.add(partition)
        status, ids = self.milvus.insert(
            collection_name=self.table_name, records=vectors, ids=keys, partition_tag=partition
        )
        print(status)
        return status

    def search(self, vector, n_results=10):
        status, results = self.milvus.search(
            collection_name=self.table_name,
            query_records=[vector],
            top_k=n_results+1, # first result is the vector itself
            params={'nprobe': 16},
        )
        return results

    def get_vectors_by_ids(self, vectors_ids):
        filtered_vector_ids = [v for v in vectors_ids if v]
        if not filtered_vector_ids:
           return []
        status, vectors = self.milvus.get_entity_by_id(self.table_name, ids=filtered_vector_ids)

        result_vectors = []
        # vectors stores list of lists
        for v_id in vectors_ids:
           vector = vectors.pop(0) if v_id else None
           result_vectors.append(vector)             
        return result_vectors


def list_milvus_tables():
    milvus = Milvus()
    milvus.connect()
    tables = milvus.show_tables()
    return tables[1]


def set_parser() -> argparse.ArgumentParser:
    """Setup parser needed for the script. No any parameters required.

    Returns
    -------
    parser
        an ArgumentParser object, from which entities filename and suffix could be extracted
    """

    parser = argparse.ArgumentParser(description="Parse engine params: ")
    parser.add_argument(
        "-e",
        "--encodedpath",
        dest="encodedpath",
        default="../data/encoded_data/",
        help="path to the directory with entities",
    )

    return parser


def index_entity(encoded_path, vectors_fn, keys_fn, table_name):
    # constants:
    dimension = 1024
    BATCH_SIZE = 500

    print(
        f"Creating table: {table_name} \n using vecs: {vectors_fn} \n and keys: {keys_fn} \n"
    )
    engine = SearchEngine(table_name, dimension=dimension)
    fvectors = open(os.path.join(encoded_path, vectors_fn), "rb")
    fkeys = open(os.path.join(encoded_path, keys_fn), "r")

    keys = []
    vectors = []
    i = 0
    for key in tqdm.tqdm(fkeys, desc=f"reading {table_name} keys"):
        vector = numpy.load(fvectors)
        vectors.append(vector.tolist())
        keys.append(i)
        i += 1
        if len(keys) >= BATCH_SIZE:
            engine.index(vectors, keys)
            keys = []
            vectors = []

    if len(keys):
        engine.index(vectors, keys)
    fvectors.close()
    fkeys.close()


def ner_prefix(use_ner: bool):
    if use_ner:
        using_ner = "with_ners"
    else:
        using_ner = "no_ners"

    return using_ner


def get_vecs_keys_files(folder, collection, ner):
    key_vec_dict = {}
    using_ner = ner_prefix(ner)
    files = [
        i
        for i in os.listdir(folder)
        if os.path.isfile(os.path.join(folder, i))
        and i.startswith(collection)
        and using_ner in i
    ]

    vec_fn, key_fn = ("", "")
    for file in files:
        if ".bin" in file:
            vec_fn = file
        elif ".txt" in file:
            key_fn = file

    key_vec_dict[f"{collection}_{using_ner}"] = {key_fn: vec_fn}

    return key_vec_dict


if __name__ == "__main__":
    # 1. Parse params
    parser = set_parser()
    args = parser.parse_args()
    encodedpath = args.encodedpath

    collections_l = [
        "exhibits",
        "gen_tree_individuals",
        "family_names",
        "movies",
        "personalities",
        "places",
    ]
    to_index_dict = {}
    print("Forming dictionary to index ...")
    for collection in collections_l:
        key_vec_dict_no_ner = get_vecs_keys_files(encodedpath, collection, False)
        key_vec_dict_ner = get_vecs_keys_files(encodedpath, collection, True)
        to_index_dict.update(key_vec_dict_ner)
        to_index_dict.update(key_vec_dict_no_ner)

    print(f"Dictionary to index:\n {to_index_dict}")
    for table_name in to_index_dict.keys():
        # check if table name already exists:
        existing_tables = list_milvus_tables()
        if table_name in existing_tables:
            print(f"{table_name} will not be processed because it's already exists")
            continue
        else:
            print(f"{table_name} will be processed")
            key_fn = list(to_index_dict[table_name])[0]
            vec_fn = to_index_dict[table_name][key_fn]
            index_entity(encodedpath, vec_fn, key_fn, table_name)

