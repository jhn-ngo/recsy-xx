### Data preparing instructions ###

1. Activate venv and move to folder with code: 

    ```bash
    source <path_to_venv>/bin/activate
    cd <path_to_recsys>/src
    ```
  
2. Download data from directus:

    ```bash
    python data_loaders/directus_loader.py -savepath ../data/source_data/
    ```

3.  Extract entities from data:
    
    ```bash
    python entities_parsers/entities_extractor.py -f ../data/source_data/ -s ../data/parsed_entities/ --ner 
    ``` 
        
4. Encode these entities using laser: 

    ```bash
   python encoder/laser_encoder.py -e ../data/parsed_entities/ -s ../data/encoded_data/
   ```
 
5. Index entities to milvus

    ```bash
   python engine/engine.py ../data/encoded_data/embeddings_exhibits_no_ners.bin ../data/encoded_data/keys_exhibits_no_ners.txt exhibits_no_ners 
   ```
