#
# Copyright (c) 2021 Pangeanic/JHN.
#
# This file is part of RecSy (EuropeanaXX).
# See https://bitbucket.org/jhn-ngo/recsy-xx for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import csv
import tqdm
import argparse


def set_parser() -> argparse.ArgumentParser:
    """Setup parser needed for the script. No any parameters required.

    Returns
    -------
    parser
        an ArgumentParser object, from which entities filename and suffix could be extracted
    """

    parser = argparse.ArgumentParser(description="Indicate input and output directories")
    parser.add_argument(
        "-i",
        "--input",
        dest="inputpath",
        default="../data/parsed_entities/",
        help="path to the directory with entities in old format",
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="outputpath",
        default="../data/entities_to_index/",
        help="path to directory to which new format files will be saved",
    )

    return parser


def rewrite_csv(infile, outfile):
    out = csv.writer(open(outfile, "w"), delimiter=",")
    out.writerow(["id", "passage"])

    with open(infile, "r") as f:
        fscv = csv.reader(f)
        for i, row in enumerate(tqdm.tqdm(fscv)):
            key, entities = row[0], ",".join(row[1:])
            out.writerow([key, entities])

    print(f"File {infile} in new format is written here: {outfile}")


if __name__ == "__main__":
    # Parse params from CLI:
    parser = set_parser()
    args = parser.parse_args()
    inputpath = args.inputpath
    outputpath = args.outputpath
    input_data_dir = os.fsencode(inputpath)

    files_to_rewrite_mapping = {
        "exhibits_entities_no_ners.csv": "exhibits.csv",
        "movies_entities_with_ners.csv": "movies.csv", 
        "places_entities_with_ners.csv": "places.csv",
        "personalities_entities_with_ners.csv": "personalities.csv",
        "gen_tree_individuals_entities_no_ners.csv": "gen_tree_individuals.csv",
        "family_names_entities_no_ners.csv": "family_names.csv"
        }


    # browse folder
    for in_fn in files_to_rewrite_mapping.keys():    
        full_in_fn = os.path.join(inputpath, in_fn)
        full_out_fn = os.path.join(outputpath, files_to_rewrite_mapping[in_fn])
        rewrite_csv(full_in_fn, full_out_fn)
    
