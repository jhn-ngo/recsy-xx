#
# Copyright (c) 2021 Pangeanic/JHN.
#
# This file is part of RecSy (EuropeanaXX).
# See https://bitbucket.org/jhn-ngo/recsy-xx for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import requests
import csv
import io


def read_gdrive_csv(return_only_users_ids=False, return_user_ids_and_names=False):
    # pre-defined file_id
    file_id = "1GmYXW6_r5Tc_JQsiwaa0ONV1UVRRNP433NOMbG4kHTg"
    # pre-defined headers
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0", "DNT": "1",
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
               "Accept-Encoding": "deflate", "Accept-Language": "en-US,en;q=0.5"}

    url = f"https://docs.google.com/spreadsheets/d/{file_id}/export?format=csv"
    r = requests.get(url, headers=headers)

    sio = io.StringIO(r.text, newline=None)
    fcsv = csv.reader(sio, dialect=csv.excel)
    if return_only_users_ids:
        row_count = sum(1 for _ in fcsv) - 1
        return [i+1 for i in range(row_count)]

    csv_f = []
    map_d = {}
    for i, row in enumerate(fcsv):
        if i == 0:
            map = row
        else:
            if return_user_ids_and_names:
                csv_f.append({"id": row[0], "username": (row[1]+" "+row[2])})
            else:
                csv_f.append(row)

    for i in map:
        map_d[i] = map.index(i)

    return csv_f, map_d


if __name__ == "__main__":
    print("csv and maps: ", read_gdrive_csv())
    print("all user_ids: ", read_gdrive_csv(True))
    print("all user_ids: ", read_gdrive_csv(False, True)[0])
