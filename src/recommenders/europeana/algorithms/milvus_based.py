from typing import List
import lmdb

import sys

sys.path.append("/opt/recsy/src")  # tmp fix to fix imports

from engines.searchers.engine import SearchEngine
from recommenders.europeana.algorithms.base import BaseRecAlgorithm

from numpy.linalg import norm
from numpy import array as np_array


class MilvusAlgorithm(BaseRecAlgorithm):
    def __init__(
        self,
        host="209.182.239.246",
        port=19530,
        index="API",
        path_to_db="",
        events=None,
    ):
        self.host = host
        self.port = port
        self.index = index
        self.client = SearchEngine(
            host=self.host, port=self.port, table_name=self.index
        )
        self.id2key_db = lmdb.Environment(path_to_db + "_id2key")
        self.key2id_db = lmdb.Environment(path_to_db + "_key2id")
        self.events = events

    def find_distance_between_two_vecs(self, a, b):
        return norm(np_array(a) - np_array(b))

    def _recommend_base(self, item_ids: List, limit=10, offset=0) -> List:
        recs = []
        n_items = len(item_ids)
        if not n_items:
            return recs

        # calculate cycle limit: for each item recommend equal amount of records
        # include offset(skip)
        cycle_limit = int(max(1, round(limit + offset + 1 / n_items, 0)))
        vectors = self.get_vectors_by_keys(item_ids)
        for vector in vectors:
            # TODO: add distance normalization
            recs += [
                (self.get_key_by_id(res.id), round(res.distance, 5))
                for row in self.client.search(vector, cycle_limit)
                for res in row
                if res.distance != 0  # to exclude the vector itself
            ]

        recs = sorted(
            list(set(recs)), key=lambda tup: tup[1]
        )  # to leave unique and sort by distance
        return recs[offset:][:limit]

    def _recommend_for_liked(self, items_ids, liked_items_ids, limit=10, offset=0):
        coefficient = 0.9
        recs_orig = self._recommend_base(items_ids, limit)
        recs_for_liked = self._recommend_base(liked_items_ids, limit + 10, offset)
        recs_for_liked = [(i[0], round(coefficient * i[1]), 5) for i in recs_for_liked]
        merged_recs = recs_orig + recs_for_liked
        merged_recs = sorted(merged_recs, key=lambda tup: tup[1])
        return merged_recs[offset:][:limit]

    def _rerank_disliked(self, items_ids, disliked_items_ids, limit=10, offset=0):
        coefficient = 0.005
        recs_orig = self._recommend_base(items_ids, limit + 10, offset)
        disliked_vectors = self.get_vectors_by_keys(disliked_items_ids)
        for disliked_vector in disliked_vectors:
            recs_orig = [
                (
                    i[0],
                    round(
                        i[1]
                        + coefficient
                        * (
                            self.find_distance_between_two_vecs(
                                disliked_vector, self.get_vectors_by_keys([i[0]])
                            )
                        ),
                        5,
                    ),
                )
                for i in recs_orig
            ]

        recs_orig = sorted(recs_orig, key=lambda tup: tup[1])
        return recs_orig[offset:][:limit]

    def _recommend(self, items_ids, limit=10, offset=0, **kwargs):
        recs = []
        final_recs = []

        vectors = kwargs.get("vectors")
        if vectors is not None:
            closest_by_labels = self.find_closest_to_vectors(vectors)
        else:
            closest_by_labels = []

        session_id = kwargs.get("session_id")

        # 1. get events
        events_list = (
            self.events.get_last_events(session_id=session_id) if self.events else []
        )
        if not events_list:  # no like or dislikes
            base_recs = self._recommend_base(items_ids, limit, offset)
            recs = sorted(base_recs + closest_by_labels, key=lambda tup: tup[1])
            final_recs = [i[0] for i in recs][:limit]
            final_recs = [
                "/" + i if not i.startswith("/") else i for i in final_recs
            ]  # proper format is with slash in the beginning
            return final_recs

        # 2. select likes and dislikes
        liked_items_ids = [i["item"] for i in events_list if i["type"] == "accept"]
        disliked_items_ids = [i["item"] for i in events_list if i["type"] == "reject"]

        if liked_items_ids and disliked_items_ids:
            # apply likes then dislikes
            recs = self._recommend_for_liked(items_ids, liked_items_ids, limit, offset)
            current_items_ids = [i[0] for i in recs]
            recs = self._rerank_disliked(
                current_items_ids, disliked_items_ids, limit, offset
            )
        else:
            if liked_items_ids:
                # 3. for likes: add new recommendations, remove dublicates and rerank
                recs = self._recommend_for_liked(
                    items_ids, liked_items_ids, limit, offset
                )

            elif disliked_items_ids:
                recs = self._rerank_disliked(
                    items_ids, disliked_items_ids, limit, offset
                )

        recs = sorted(recs + closest_by_labels, key=lambda tup: tup[1])

        final_recs = [i[0] for i in recs][:limit]
        final_recs = [
            "/" + i if not i.startswith("/") else i for i in final_recs
        ]  # proper format is with slash in the beginning
        return final_recs

    def find_closest_to_vectors(self, vectors: list, limit=10):
        recs = []
        n_vectors = len(vectors)
        cycle_limit = int(max(1, round(limit + 1 / n_vectors, 0)))
        for vector in vectors:
            recs += [
                (self.get_key_by_id(res.id), round(res.distance, 5))
                for row in self.client.search(vector, cycle_limit)
                for res in row
                if res.distance != 0  # to exclude the vector itself
            ]

        recs = sorted(
            list(set(recs)), key=lambda tup: tup[1]
        )  # to leave unique and sort by distance
        return recs[:limit]

    def get_vectors_by_keys(self, keys):
        keys = [
            i for i in keys if i is not None
        ]  # exclude None keys (case when recommend by labels, not items)
        keys = [
            i.lstrip("/") for i in keys
        ]  # because in ftp we received from europeana all records start with no slash
        record_ids = [self.get_id_by_key(key) for key in keys]
        vectors = self.client.get_vectors_by_ids(vectors_ids=record_ids)
        return [i for i in vectors if i is not None]

    def get_id_by_key(self, key):
        with self.key2id_db.begin(write=False) as txn:
            record_id = txn.get(bytes(key, encoding="utf-8"))
            if record_id:
                record_id = int(record_id.decode(encoding="utf-8"))
        return record_id

    def get_key_by_id(self, record_id):
        with self.id2key_db.begin(write=False) as txn:
            record_key = txn.get(bytes(str(record_id), encoding="utf-8"))
            if record_key:
                record_key = record_key.decode(encoding="utf-8")
        return record_key


if __name__ == "__main__":
    from pprint import pprint
    import sys

    # examples:
    # 00101/00272FBF35C5983D3A2F5C591DB01ECCF348E47B
    # 00101/001E57281EEC5BFA79D5FD2EF692C8E1FC26C511
    # python recommenders/europeana/algorithms/milvus_based.py 00101/00272FBF35C5983D3A2F5C591DB01ECCF348E47B

    rec_algo = MilvusAlgorithm(index="API_records", path_to_db="st_prod")
    europeana_prefix = "https://www.europeana.eu/en/item/"
    item_id = sys.argv[1:]
    print(item_id)
    res = rec_algo.recommend(item_id, limit=5, offset=1)
    # res = [(i[0], i[1], "https://www.europeana.eu/en/item/"+i[0]) for i in res]
    print(f"Looking for recommendations for item: {europeana_prefix+item_id[0]}")
    print("--- no session bsaed recommendation ---")
    pprint([europeana_prefix + i for i in res])
    print("----- with likes and dislikes ----------")
    res_likes = rec_algo.recommend(item_id, limit=5, offset=1, session_id=1)
    pprint([europeana_prefix + i for i in res_likes])
