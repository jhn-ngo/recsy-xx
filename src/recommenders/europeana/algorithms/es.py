import json
import random
from typing import List
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl import Q
from elasticsearch_dsl.query import MoreLikeThis
from Levenshtein import distance

from recommenders.europeana.algorithms.base import BaseRecAlgorithm


class ESRecAlgorithm(BaseRecAlgorithm):
    def __init__(
        self, es_host="209.182.239.246", es_port=3200, es_index="europeana_data_v3"
    ):
        self.host = es_host
        self.port = es_port
        self.index = es_index
        self.client = Elasticsearch([{"host": self.host, "port": self.port}])

    def _recommend(self, item_ids: List, limit=10, offset=0) -> List:
        recs = []
        n_items = len(item_ids)
        cycle_limit = int(max(1, round(limit / n_items, 0)))
        for item in item_ids:
            # tmp fix:
            if item.startswith("/"):
                item = item[1:]
            recs += self._get_unique_results(
                self.index, item, cycle_limit, return_ids=True
            )

        recs = list(set(recs))  # to leave unique

        return recs[:limit]
        # return self._get_unique_results(self.index, item_ids[0], limit, return_ids=True)

    def _get_more_like_this(self, index, id, n_start=0, n_end=10, return_ids=False):
        """Extracting items similar to the item with provided id using more like this query"""

        s = Search(using=self.client, index=index)
        s = s.query(
            MoreLikeThis(
                like=[{"_index": index, "_id": id}],
                fields=["title", "description", "creator", "tags", "places", "times"],
                min_term_freq=1,
                max_query_terms=12,
            ),
        )[n_start:n_end]

        response = s.execute()
        response = response.to_dict()

        return response["hits"]["hits"]

    def _filter_duplicates(self, es_res):
        """Filtering duplicates based on Levenstein distance. Returns filterd list of hits"""

        unique_strings = []
        dublicated_ids = []
        for i, item in enumerate(es_res):
            # data_string = " ".join(item["_source"]["title"]) + " ".join(
            #    item["_source"]["description"]
            # )
            title = item["_source"].get("title", "") or ""
            description = item["_source"].get("description", "") or ""
            data_string = title + " " + description
            threshold = 0.1 * len(
                data_string
            )  # 10% of row length - this part needs to be improved
            if i == 0:
                unique_strings.append(data_string)
            else:
                distances = [
                    distance(data_string, unique_string)
                    for unique_string in unique_strings
                ]
                if min(distances) > threshold:
                    unique_strings.append(data_string)
                else:
                    dublicated_ids.append(item["_id"])

        es_res = [item for item in es_res if item["_id"] not in dublicated_ids]

        return es_res

    def _get_unique_results(self, index, id, n_results, return_ids=False):
        """Function return n unique hits"""

        n_start = 0
        n_end = n_results
        search_res = self._filter_duplicates(
            self._get_more_like_this(index, id, n_start, n_end)
        )

        while len(search_res) < n_results:
            n_start = n_end
            n_end = n_end + 10
            new_search_res = self._get_more_like_this(index, id, n_start, n_end)
            if len(new_search_res) == 0:
                break
            search_res += new_search_res
            search_res = self._filter_duplicates(search_res)

        if return_ids:
            search_res = [i["_id"] for i in search_res]

        return search_res[:n_results]

    def _create_translations_field(self, hit) -> dict:
        translations = {}
        languages = [
            "en",
            "de",
            "fr",
            "it",
            "es",
            "nl",
            "pl",
            "sv",
            "cs",
            "ca",
            "el",
            "he",
        ]
        for lang in languages:

            if hit.get(f"title_{lang}"):
                if lang not in translations.keys():
                    translations[lang] = {}
                translations[lang].update({"title": hit[f"title_{lang}"]})

            if hit.get(f"description_{lang}"):
                if lang not in translations.keys():
                    translations[lang] = {}
                translations[lang].update({"description": hit[f"description_{lang}"]})

        return translations

    def _parse_es_results(self, results_dict):
        results = []
        scores = []

        for hit in results_dict:
            score = hit["_score"]
            # special link for american sets (temporary solution)
            if hit["_source"]["dataset"] in [
                # "YIVO_JE",
                # "LBI_books",
                # "LBI_ms",
                # "LBI_periodicals",
                # "LBI_art",
                # "LBI_photos",
                # "AJHS_text",
                # "AJHS_photographs",
                # "YUM_all",
                # "ASF_all",
            ]:
                tier = hit["_source"]["tier"]
                if not tier:
                    continue
                image_url = (
                    f"https://cms-data.j-story.org/assets/{hit['_source']['image']}"
                )
            else:
                image_url = hit["_source"]["original_image_url"]
                if (
                    hit["_source"]["dataset"] == "TNN"
                ):  # tmp to show TNN data (no images)
                    image_links = [
                        "https://cms.j-story.org/assets/1e9914c1-356d-4f1f-bbf1-94c1992b9b0d",
                        "https://cms.j-story.org/assets/8d6286aa-0537-4c5a-806b-d19c61a783d7",
                        "https://cms.j-story.org/assets/ab3d050f-d381-44c9-9eec-1f484bb46eb0",
                        "https://cms.j-story.org/assets/5e130e61-291e-4efa-ab57-803efe9b5e9d",
                        "https://cms.j-story.org/assets/71b8b002-b792-44f7-b81c-7c8644582d64",
                        "https://cms.j-story.org/assets/70f5f5f6-cbe6-48a5-a1f7-773233042af6",
                    ]
                    image_url = random.choice(image_links)
                if not image_url:
                    continue
                if image_url.endswith(".html"):  #  tmp fix for NLI
                    continue
                try:
                    image_url = json.loads(image_url)
                    if isinstance(image_url, list):
                        image_url = image_url[0]
                        if (
                            "@rdf:resource" in image_url
                        ):  # tmp fix for AIU double shownby
                            image_url = image_url["@rdf:resource"]
                except:
                    pass

            location = hit["_source"]["location"]
            geocoordinates = hit["_source"].get("geocoordinates", [])
            if geocoordinates:
                if hit["_source"]["dataset"] == "TNN":  # tmp for demo
                    random_lat_noise = round(random.uniform(0.001, 0.009), 4)
                    random_lon_noise = round(random.uniform(0.001, 0.009), 4)

                    geocoordinates = [
                        {
                            "lat": round((float(i["lat"]) + random_lat_noise), 4),
                            "lon": round((float(i["lon"]) + random_lon_noise), 4),
                            "label": i["label"],
                        }
                        for i in geocoordinates
                    ]

            translations = self._create_translations_field(hit["_source"])
            tags = hit["_source"]["tags"]
            if tags:
                tags = list(set(tags))
            else:
                tags = []

            res_dict = {
                "id": hit["_id"],
                "score": round(float(hit["_score"]), 3),
                "title": hit["_source"]["title"],
                # "title_en": hit["_source"].get("title_en", ""),
                "description": hit["_source"]["description"],
                # "description_en": hit["_source"].get("description_en", ""),
                "language": hit["_source"].get("language", ""),
                "image_url": image_url,
                # "preview_url": image_url,
                "type": hit["_source"]["media_type"],
                "tags": tags,
                "provider": {
                    "id": hit["_source"]["provider"],
                    "logo": f"https://cms-data.j-story.org/assets/{hit['_source']['provider_logo']}",
                },
                "rights": {
                    "url": hit["_source"]["rights_url"],
                    # "icon": hit["_source"]["rights_icon"],
                },  # TODO: add icons as well
                "places": location,
                "locations": geocoordinates,
                "geo_point": hit["_source"].get("geo_point", {}),
            }

            if translations:
                res_dict["translations"] = translations

            try:
                last_item_score = scores[-1]
                last_item_title = results[-1]["title"]
            except IndexError:
                last_item_score = 0
                last_item_title = ""

            if (
                score == last_item_score
                and res_dict.get("title", "no title") == last_item_title
            ):
                # if scores and titles are the same as in previous item it should be collapsed into similar_items key
                if "similar_items" not in results[-1]:
                    results[-1]["similar_items"] = []
                results[-1]["similar_items"].append(res_dict)
            else:
                results.append(res_dict)
                scores.append(score)

        return results

    def _query_text_search(self, query_text="", limit=10, offset=0, **kwargs):
        """Function return search results for text search"""

        results = {"results": []}

        query_text = query_text.strip()
        if not query_text:
            return results

        # parse kwargs here
        geolocated = kwargs.get("geolocated", False)
        languages = kwargs.get("languages", [])
        media_types = kwargs.get("media_types", [])
        rights = kwargs.get("rights", [])

        s = Search(using=self.client, index=self.index)
        search_dict = {
            "query": {
                "query_string": {
                    "query": query_text,
                    # "default_operator": "AND",
                    "fields": ["title*^8", "description*^4", "provider^2"],
                    "fuzziness": "AUTO",
                }
            },
            "size": limit,
            "from": offset,
        }

        s.update_from_dict(search_dict)

        if geolocated:
            s = s.filter(Q("exists", field="geocoordinates"))
        if languages:
            s = s.filter(Q("terms", language=languages))
        if media_types:
            allowed_media_types = ["text", "image", "video", "sound", "3d"]
            media_types = [
                i.lower() for i in media_types if i.lower() in allowed_media_types
            ]
            if media_types:
                s = s.filter(Q("terms", media_type=media_types))
        if rights:
            s = s.filter("terms", rights_url__keyword=rights)
            # s = s.filter(Q("match", rights_url__keyword="http://creativecommons.org/publicdomain/mark/1.0/"))

        # print(s.to_dict())

        response = s.execute().to_dict()
        total = s.count()
        response = response["hits"]["hits"]
        # print(f"Response: {response}")
        results["results"] = self._parse_es_results(response)
        results["total"] = total
        return results

    def _multi_match_search(self, query_text="", limit=10, offset=0, geolocated=False):
        """ "Function return results fuzzy search based on title, description and provider
        Title have weight: 8
        Description weight: 4
        Provider: 2
        """

        results = {"results": []}

        s = Search(using=self.client, index=self.index)

        search_dict = {
            "query": {
                "function_score": {
                    "query": {
                        "multi_match": {
                            "query": query_text,
                            "fields": ["title*^8", "description*^4", "provider^2"],
                            "fuzziness": "AUTO",
                            "prefix_length": 0,
                        }
                    }
                }
            },
            "size": limit,
            "from": offset,
        }

        s.update_from_dict(search_dict)

        if geolocated:
            s = s.filter(Q("exists", field="geocoordinates"))

        response = s.execute().to_dict()
        total = s.count()
        response = response["hits"]["hits"]
        results["results"] = self._parse_es_results(response)
        results["total"] = total
        # print(f"RESULTS: {results}")

        return results

    def _geo_search(
        self, geo_point: dict, distance_km=10, limit=10, offset=0, **kwargs
    ):
        """Function returns search results based on geo point"""
        results = {"results": []}

        if not geo_point:
            return results

        # parse kwargs here
        geolocated = kwargs.get("geolocated", False)
        languages = kwargs.get("languages", [])
        media_types = kwargs.get("media_types", [])
        rights = kwargs.get("rights", [])

        s = Search(using=self.client, index=self.index)
        search_dict = {
            "query": {
                "bool": {
                    "must": {"match_all": {}},
                    "filter": {
                        "geo_distance": {
                            "distance": f"{distance_km}km",
                            "geo_point": {
                                "lat": geo_point["lat"],
                                "lon": geo_point["lon"],
                            },
                        }
                    },
                }
            },
            "size": limit,
            "from": offset,
        }

        s.update_from_dict(search_dict)

        if geolocated:
            s = s.filter(Q("exists", field="geocoordinates"))
        if languages:
            s = s.filter(Q("terms", language=languages))
        if media_types:
            allowed_media_types = ["text", "image", "video", "sound", "3d"]
            media_types = [
                i.lower() for i in media_types if i.lower() in allowed_media_types
            ]
            if media_types:
                s = s.filter(Q("terms", media_type=media_types))
        if rights:
            s = s.filter("terms", rights_url__keyword=rights)

        response = s.execute().to_dict()
        total = s.count()
        response = response["hits"]["hits"]
        # print(f"Response: {response}")
        results["results"] = self._parse_es_results(response)
        results["total"] = total
        return results

    def _get_records_from_ids(self, ids_list):
        """Function return records from ids"""

        results = {"results": []}
        s = Search(using=self.client, index=self.index)
        s.update_from_dict({"query": {"ids": {"values": ids_list}}})
        response = s.execute().to_dict()
        total = s.count()
        response = response["hits"]["hits"]
        results["results"] = self._parse_es_results(response)
        results["total"] = total

        return results
