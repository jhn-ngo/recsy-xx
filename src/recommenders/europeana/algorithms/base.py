from typing import List

class BaseRecAlgorithm:

    def recommend(self, in_items, limit=10, offset=0, **kwargs) -> List:
        if not isinstance(in_items, (list, tuple)):
            in_items = [in_items]

        return self._recommend(in_items, limit, offset, **kwargs)

    def _recommend(self, item_ids: List, limit=10, offset=0) -> List:
        raise Exception("Child class should implement this method")