import re
import random
from typing import List
from recommenders.europeana.algorithms.base import BaseRecAlgorithm
from engines.searchers.europeana.api import EuropeanaAPI
import cleantext


class EuropeanaSearchRecAlgorithm(BaseRecAlgorithm):

    def __init__(self, key, url):
        self.eusearch = EuropeanaAPI(key, url)


    def _recommend(self, all_item_ids: List, limit=10, offset=0) -> List:
        # Pick random input items
        item_ids = all_item_ids
        if len(item_ids) > limit:
            item_ids = random.choices(item_ids, k=limit)

        offset = offset+1 # in europeana API it start from 1

        recs = []
        in_items = self.eusearch.search_ids(item_ids)['results']
        n_items = len(in_items)
        if n_items == 0: return []
        # 1. Run exact queries
        recs += self._query_similar_to_items(all_item_ids, item_ids, in_items, limit, offset)
        if len(recs) >= limit: return recs
        # 2. Try relaxing the query by inserting OR between query words but limit search to titles only
        limit = limit - len(recs)
        recs += self._query_similar_to_items(all_item_ids, item_ids, in_items, limit, offset, query_hook=lambda query: "title:" + re.sub('\s+', " OR ", cleantext.remove_punct(query).strip()))
        if len(recs) >= limit: return recs
        # 3. Try relaxing the query by inserting OR between query words
        limit = limit - len(recs)
        recs += self._query_similar_to_items(all_item_ids, item_ids, in_items, limit, offset, query_hook=lambda query: re.sub('\s+', " OR ", cleantext.remove_punct(query).strip()))
        if len(recs) >= limit: return recs
        # Filter duplicated and input items while preserving original list order
        seen = set(all_item_ids)
        seen_add = seen.add
        return [rec for rec in recs if not (rec in seen or seen_add(rec))]

    def _get_query(self, in_item):
        query = "*"
        if 'dcTitleLangAware'  in in_item:
            if 'def' in in_item['dcTitleLangAware']:
                query = in_item['dcTitleLangAware']['def']
            else:
                # Pick any available language
                for key,value in in_item['dcTitleLangAware'].items():
                    query = value
                    break
        elif 'title' in in_item:
            query = in_item['title'][:200] # limiting title size to 200 characters since it fails on very long titles
        if isinstance(query, (list,tuple)):
            query = " ".join(query)
        query = cleantext.clean(query)
        return query

    def _query_similar_to_items(self, all_item_ids, item_ids, in_items, limit, offset, query_hook=None):
        recs = []

        combined_query = []
        for in_item in in_items:
            query = self._get_query(in_item)
            if query_hook:
                query = query_hook(query)
            combined_query.append(query)
        recs += self.eusearch.search(query=" OR ".join(combined_query), rows=2*limit, start=offset)['results']
        return [rec['id'] for rec in recs if rec['id'] not in all_item_ids][:limit]


if __name__ == "__main__":
    from pprint import pprint
    import sys, os

    sys.path.append("/opt/recsy/src")  # tmp fix to fix imports

    RECSY_SEARCH_KEY = os.getenv("RECSY_SEARCH_KEY")
    RECSY_SEARCH_API = os.getenv("RECSY_SEARCH_API") or "https://api.europeana.eu/record"
    RECSY_USER_SETS_API = os.getenv("RECSY_USER_SETS_API") or "https://api.europeana.eu/set"

    rec_algo = EuropeanaSearchRecAlgorithm(RECSY_SEARCH_KEY, RECSY_SEARCH_API)
    #pprint(eusearch.search(sys.argv[1], rows=3))
    pprint(rec_algo.recommend(sys.argv[1:], limit=5, offset=1))
    