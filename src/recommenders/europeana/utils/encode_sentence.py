import sys
from sentence_transformers import SentenceTransformer
import time

model = SentenceTransformer('xlm-r-100langs-bert-base-nli-stsb-mean-tokens')

f = open(sys.argv[1], "r")
sentences = f.readlines()

i = 0; 
start = time.time()
while i < len(sentences):
  start_c = time.time()
  print(i)
  emb = model.encode(sentences[i:i+1000])
  end_c = time.time()
  i += 1000
  print(i, end_c - start_c)
end = time.time()
print(end - start)
