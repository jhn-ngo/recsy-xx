import os
import json
import requests
import time
from zipfile import ZipFile
import numpy
import aiohttp
import asyncio


PATH_TO_ZIPS = "parsed_records/zips/"
PATH_TO_UNZIPPED_FILES = "tmp_encoding/"
PATH_TO_ENCODED_FILES = "encoded_records/"
BATCH_SIZE = 100


def unzip_all(path_to_zips, path_to_tmp):
    i = 0
    for fzip_name in os.listdir(path_to_zips):
        i += 1
        print(f"Unzipping {i}-th zip")
        # Create a ZipFile Object and load sample.zip in it
        with ZipFile(path_to_zips + fzip_name, "r") as zipObj:
            # Extract all the contents of zip file in different directory
            zipObj.extractall(path_to_tmp)


def encode_records(records, ip="35.233.235.237"):
    port = 5001
    url = "http://" + ip + ":" + str(port) + "/embedding_api/embedding"
    print(url)

    result = requests.post(
        url,
        data=json.dumps({"record_data": records, "model": "laser"}),
        headers={"Content-Type": "application/json", "Accept": "application/json"},
    ).json()

    embeds_nn = result["data"]["output_embeddings"]
    embeds_laser = result["data"]["input_embeddings"]

    return (embeds_nn, embeds_laser)


async def get_encodings(session, url, records):
    async with session.post(
        url,
        data=json.dumps(
            {
                "record_data": records,
                "model": "laser",
                "return_fields": ["output_embeddings"],
            }
        ),
        headers={"Content-Type": "application/json", "Accept": "application/json"},
    ) as response:
        return response.json()


def get_records_to_encode(path_to_raw_records=PATH_TO_UNZIPPED_FILES):
    for dir_w_jsons in sorted(os.listdir(path_to_raw_records)):
        path_to_dir_w_jsons = os.path.join(path_to_raw_records, dir_w_jsons)
        dir_to_write = os.path.join(PATH_TO_ENCODED_FILES, dir_w_jsons)
        for f_json in sorted(os.listdir(path_to_dir_w_jsons)):
            records_to_encode = []
            keys = []

            with open(os.path.join(path_to_dir_w_jsons, f_json), "r") as json_file:
                data = json.load(json_file)

            # extracting record:
            for rec in data:
                key_id = rec.pop("id", None)
                keys.append(key_id)
                records_to_encode.append(json.dumps(rec))

            assert len(keys) == len(records_to_encode)

            yield (keys, records_to_encode, dir_to_write)


async def main():
    api_ips = ["35.233.235.237", "35.198.128.141", "34.69.222.120", "34.122.131.62"]
    ip_counter = 0
    responses = []
    all_keys = []
    async with aiohttp.ClientSession() as session:
        for keys, records_to_encode, dir_to_write in get_records_to_encode(path_to_raw_records=PATH_TO_UNZIPPED_FILES):

            if not os.path.exists(dir_to_write):
                os.makedirs(dir_to_write)

            print(f"Dir to write: {dir_to_write}")

            ip_to_encode = api_ips[ip_counter]
            url = "http://" + ip_to_encode + ":5001/embedding_api/embedding"

            # if responses[ip_to_encode]:
            #     json = await responses[ip_to_encode]  # process to milvus here?

            res = get_encodings(session, url, records_to_encode)
            responses.append(res)

            embed_nn_fn = os.path.join(dir_to_write, "embeds_nn.bin")
            # embed_laser_fn = os.path.join(dir_to_write, "embeds_laser.bin")
            keys_fn = os.path.join(dir_to_write, "keys.txt")

            fembed_nn = open(embed_nn_fn, "wb")
            # fembed_laser = open(embed_laser_fn, "wb")
            fkeys = open(keys_fn, "w")

            all_keys += keys

            if ip_counter >= 3:

                print(f"Responses: {responses}")
                vector_responses = await asyncio.gather(*responses)
                print(f"ip counter: {ip_counter}, vector responses: {vector_responses}")

                # writing:
                for narray in vector_responses["data"]["output_embeddings"]:
                    numpy.save(fembed_nn, narray)

                # for narray in embeds_laser:
                #     numpy.save(fembed_laser, narray)

                for key in all_keys:
                    fkeys.write(key + "\n")

                # zero
                responses = []
                all_keys = []
                ip_counter = 0

            ip_counter += 1


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

###########
# def main_old():
#     # Iterate over unzipped json
#     for dir_w_jsons in os.listdir(PATH_TO_UNZIPPED_FILES):
#         start = time.time()
#         path_to_dir_w_jsons = os.path.join(PATH_TO_UNZIPPED_FILES, dir_w_jsons)
#         api_ips = ["35.233.235.237", "35.198.128.141", "34.69.222.120", "34.122.131.62"]
#         ip_counter = 0

#         dir_to_write = os.path.join(PATH_TO_ENCODED_FILES, dir_w_jsons)
#         print(f"Dir to write encodings for {dir_w_jsons} is: {dir_to_write}")
#         if not os.path.exists(dir_to_write):
#             os.makedirs(dir_to_write)

#         embed_nn_fn = os.path.join(dir_to_write, "embeds_nn.bin")
#         embed_laser_fn = os.path.join(dir_to_write, "embeds_laser.bin")
#         keys_fn = os.path.join(dir_to_write, "keys.txt")

#         print(f"Generate files will be: {embed_nn_fn}, {embed_laser_fn}, {keys_fn}")

#         fembed_nn = open(embed_nn_fn, "wb")
#         fembed_laser = open(embed_laser_fn, "wb")
#         fkeys = open(keys_fn, "w")

#         print("Iterating over jsons in the folder: ")

#         for f_json in os.listdir(path_to_dir_w_jsons):
#             ip_counter += 1
#             if ip_counter > 3:
#                 ip_counter = 0

#             ip_to_encode = api_ips[ip_counter]

#             records_to_encode = []
#             keys = []

#             with open(os.path.join(path_to_dir_w_jsons, f_json), "r") as json_file:
#                 data = json.load(json_file)

#             # extracting record:
#             for rec in data:
#                 key_id = rec.pop("id", None)
#                 keys.append(key_id)
#                 records_to_encode.append(json.dumps(rec))

#             assert len(keys) == len(records_to_encode)

#             # receiving emebedings
#             embeds_nn, embeds_laser = encode_records(records_to_encode, ip_to_encode)

#             print(
#                 f"Time to encode 100 records from {f_json} took: {round(time.time()-start, 0)} seconds"
#             )

#             # writing:
#             for narray in embeds_nn:
#                 numpy.save(fembed_nn, narray)

#             for narray in embeds_laser:
#                 numpy.save(fembed_laser, narray)

#             for key in keys:
#                 fkeys.write(key + "\n")


#########


# start = time.time()


# records = [
#     '{"title": ["Veronica spicata L."], "description": ["lalal"], "creator": ["something here"], "tags": [], "places": ["in collibus Silefine."], "times": ["0101"]}',
#     '{"title": ["Veronica spicata L."], "description": [], "creator": [], "tags": [], "places": ["Italy"], "times": []}',
# ]
# result = requests.post(
#     "http://35.233.235.237:5001/embedding_api/embedding",
#     data=json.dumps({"record_data": records, "model": "laser"}),
#     headers={"Content-Type": "application/json", "Accept": "application/json"},
# ).json()

# # print(result)
# print(f"Results obtained in {round(time.time()-start, 3)} seconds")

# print("----")
# print(result["data"].keys())

# print(len(result["data"]["output_embeddings"][0]))
# print(len(result["data"][0]))
# print(len(result["data"][1]))
