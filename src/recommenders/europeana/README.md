## Recommendations approach


For each item we are looking for vectors which are close to the vector which represent this item. 
If recommendation is done for the set (group of items) - then we looking for the vectors which are close to each element of the set and then rerank them by distance score. 

In case vectorized approach couldn't find recommendations then we are looking for similar items using Europeana Search API.

### Session-based recommendation

Inside session user provide us with a useful feedback: accept (like) or reject (dislike) of the provided recommendations.

If user likes an item - then we are looking for the vectors which are close to liked one and then rerank such vectors among these and generic recommendation by distance score. 

If user dislikes an item - then we rerank generic recommendation based on distance from each potential recommendation to the vector of disliked item.

If user provides us with both positive and negative feedback then by first step we make our potential recommendation broader by adding vectors which are similar to the liked ones and then rerank them by distance to the disliked.

