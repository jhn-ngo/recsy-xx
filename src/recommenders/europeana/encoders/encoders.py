import os
import json
import requests
import gzip
import numpy
import argparse
import time
import concurrent
from concurrent.futures import ThreadPoolExecutor
import numpy as np


class FileRecordIterator:
    def __init__(self, path):
        self.path = path

    def __call__(self):
        for dir_w_jsons in sorted(os.listdir(self.path)):
            path_to_dir_w_jsons = os.path.join(self.path, dir_w_jsons)
            # dir_to_write = os.path.join(self.path, dir_w_jsons)
            for f_json in sorted(os.listdir(path_to_dir_w_jsons)):
                records_to_encode = []
                keys = []

                # with open(os.path.join(path_to_dir_w_jsons, f_json), "r") as json_file:
                #    data = json.load(json_file)
                with gzip.GzipFile(
                    os.path.join(path_to_dir_w_jsons, f_json), "r"
                ) as fin:
                    data = json.loads(fin.read().decode("utf-8"))

                # extracting record:
                for rec in data:
                    key_id = rec.pop("id", None)
                    keys.append(key_id)
                    records_to_encode.append(json.dumps(rec))

                assert len(keys) == len(records_to_encode)

                yield (keys, records_to_encode, dir_w_jsons)


class BaseEncoder:
    def __init__(self, record_iter, out_path):
        self.record_iter = record_iter
        self.out_path = out_path

    def encode(self, limit=None):
        i = 0
        start = time.time()

        for keys, records_to_encode, dir_to_write in self.record_iter():
            full_out_dir = os.path.join(self.out_path, dir_to_write)
            if not os.path.exists(full_out_dir):
                os.makedirs(full_out_dir)
            embed_nn_fn = os.path.join(full_out_dir, "embeds_nn.bin")
            keys_fn = os.path.join(full_out_dir, "keys.txt")

            encoded_records = self._encode(records_to_encode)
            i += len(encoded_records)
            # Write vectors and keys
            fembed_nn = open(embed_nn_fn, "ab")
            fkeys = open(keys_fn, "a")
            for key in keys:
                fkeys.write(key + "\n")
            for narray in encoded_records:
                numpy.save(fembed_nn, narray)
            fembed_nn.close()
            fkeys.close()
            print(f"Encoded {i} records")
            if limit is not None and i >= limit:
                print(f"Reached limit {limit} - stopping")
                break

        end = time.time()
        print(f"Encoding took {end - start} seconds")
    def _encode(self, records):
        raise Exception("Child class should implement this method")

    def _iter_records(self):
        raise Exception("Child class should implement this method")


class ApiEncoder(BaseEncoder):
    ENCODER_IPS = ["35.233.235.237", "35.198.128.141", "34.69.222.120", "34.122.131.62"]

    def _encode(self, records):
        batch = int(len(records) / len(self.ENCODER_IPS))
        encoded_records = []
        with ThreadPoolExecutor(max_workers=len(self.ENCODER_IPS)) as executor:
            i = 0
            result_futures = []
            while i < len(records):
                result_futures.append(
                    executor.submit(
                        self._encode_api,
                        records[i : i + batch],
                        self.ENCODER_IPS[int(i / batch)],
                    )
                )
                i += batch
            for future in concurrent.futures.as_completed(result_futures):
                encoded_records += future.result()
        return encoded_records

    def _encode_api(self, records, encoder_ip):
        port = 5001
        url = "http://" + encoder_ip + ":" + str(port) + "/embedding_api/embedding"
        print(url)

        result = requests.post(
            url,
            data=json.dumps(
                {
                    "record_data": records,
                    "model": "laser",
                    "return_fields": ["output_embeddings"],
                }
            ),
            headers={"Content-Type": "application/json", "Accept": "application/json"},
        ).json()

        embeds_nn = result["data"]["output_embeddings"]
        return embeds_nn


class BPEmbEncoder(BaseEncoder):
    def __init__(self, records_iter, out_path):
        super().__init__(records_iter, out_path)

        from bpemb import BPEmb

        self.multibpemb = BPEmb(lang="multi", vs=100000, dim=300)

        from fse.models import Average, SIF
        self.fse = Average(self.multibpemb.emb)

    def _encode(self, records):
        return self._encode_fse(records) #[self._encode_record(e) for e in records]

    def _encode_record(self, record):
        subword_embeds = self.multibpemb.embed(str(record))
        return np.mean(subword_embeds, axis=0)

    def _encode_fse(self, records):
        from fse import IndexedList
        return self.fse.infer(IndexedList([self._encode_record_fse(r) for r in records]))

    def _encode_record_fse(self, record):
        encoded_record = self.multibpemb.encode(str(record))
        return encoded_record

class LaserEncoder(BaseEncoder):
    def __init__(self, records_iter, out_path):
        super().__init__(records_iter, out_path)

        from laserembeddings import Laser
        self.laser = Laser(embedding_options={"max_sentences": 100})

    def _encode(self, records):
        return self.laser.embed_sentences(records, lang="en")

class SentenceTransformersEncoder(BaseEncoder):
    def __init__(self, records_iter, out_path):
        super().__init__(records_iter, out_path)

        from sentence_transformers import SentenceTransformer
        self.model = SentenceTransformer('xlm-r-100langs-bert-base-nli-stsb-mean-tokens')

    def _encode(self, records):
        return self.model.encode(records)


ENCODERS = {
    "api": ApiEncoder,
    "bpemb": BPEmbEncoder,
    "laser": LaserEncoder,
    "sentencetransformers": SentenceTransformersEncoder
}


def parse_args():
    parser = argparse.ArgumentParser(description="Encode Europeana-like records")
    parser.add_argument(
        "--input", "-i", help="Input directory with gzipped JSONs of records",
        required=True
    )
    parser.add_argument(
        "--output",
        "-o",
        help="Output directory where encoded records will be written",
        default="encoded_records",
    )
    parser.add_argument(
        "--encoder",
        "-e",
        choices=["api", "bpemb", "laser", "sentencetransformers"],
        required=True,
        help="Choose your encoder",
    )
    parser.add_argument(
        "--limit", "-l", type=int, help="Limit encoding to given number of records"
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    records_iter = FileRecordIterator(args.input)
    encoder = ENCODERS[args.encoder](records_iter, args.output)
    encoder.encode(args.limit)
