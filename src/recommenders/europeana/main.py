import configparser
import requests
import logging
import random
import jwt
from datetime import datetime
from multiprocessing import cpu_count
from sanic import Sanic, Blueprint
from sanic.response import text, json, empty
from sanic.exceptions import ServerError, abort
from sanic.views import HTTPMethodView
from sanic_openapi import doc, swagger_blueprint
from sanic_cors import CORS
from iso639 import languages
import sys
import os
from pathlib import Path

sys.path.append("/opt/recsy/src")  # tmp fix to fix imports
from recommenders.europeana.algorithms.es import ESRecAlgorithm
from recommenders.europeana.algorithms.eusearch import EuropeanaSearchRecAlgorithm
from recommenders.europeana.algorithms.milvus_based import MilvusAlgorithm
from recommenders.europeana.events import ESEvents, SQLEvents

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(message)s", level=logging.DEBUG)

# Path to config
path_to_app_config = os.getenv("RECSY_CONFIG_PATH") or "settings/recsy_config.ini"

config = configparser.ConfigParser()
config.read(path_to_app_config)

RECSY_SEARCH_KEY = config["app:search"].get("RECSY_SEARCH_KEY")
RECSY_SEARCH_API = config["app:search"].get(
    "RECSY_SEARCH_API", "https://api.europeana.eu/record"
)
RECSY_USER_SETS_API = config["app:search"].get(
    "RECSY_USER_SETS_API", "https://api.europeana.eu/set"
)
RECSY_EMBEDDINGS_API = config["app:search"].get(
    "RECSY_EMBEDDINGS_API", "http://85.214.245.200:4997"
)

# events are stored in sql (postgres) or
RECSY_EVENTS_PROVIDER = config["app:events"].get("RECSY_EVENTS_PROVIDER")
if not RECSY_EVENTS_PROVIDER or RECSY_EVENTS_PROVIDER not in ["sql", "elasticsearch"]:
    raise (
        Exception(
            "Specify config variable RECSY_EVENTS_PROVIDER (allowed values are sql or elasticsearch"
        )
    )

if RECSY_EVENTS_PROVIDER == "sql":
    RECSY_EVENTS_URL = config["app:events"].get("RECSY_EVENTS_URL", "localhost")
    RECSY_EVENTS_PORT = int(config["app:events"].get("RECSY_EVENTS_PORT", 5432))
    RECSY_EVENTS_SQL_DIALECT = config["app:events"].get(
        "RECSY_EVENTS_SQL_DIALECT", "postgres"
    )
    RECSY_EVENTS_DB_USER = config["app:events"].get("RECSY_EVENTS_DB_USER", "")
    RECSY_EVENTS_DB_PASSWORD = config["app:events"].get("RECSY_EVENTS_DB_PASSWORD", "")
    RECSY_EVENTS_DB_NAME = config["app:events"].get("RECSY_EVENTS_DB_NAME", "events")
    # db_connection_string: https://docs.sqlalchemy.org/en/14/core/engines.html
    # dialect+driver://username:password@host:port/database
    if RECSY_EVENTS_SQL_DIALECT == "postgres":
        dialect_driver = "postgresql+psycopg2"
    else:
        dialect_driver = RECSY_EVENTS_SQL_DIALECT

    events = SQLEvents(
        db_connection_string=f"{dialect_driver}://{RECSY_EVENTS_DB_USER}:{RECSY_EVENTS_DB_PASSWORD}@{RECSY_EVENTS_URL}:{RECSY_EVENTS_PORT}/{RECSY_EVENTS_DB_NAME}"
    )


elif RECSY_EVENTS_PROVIDER == "elasticsearch":
    RECSY_EVENTS_URL = config["app:events"].get("RECSY_EVENTS_URL", "localhost")
    RECSY_EVENTS_PORT = int(config["app:events"].get("RECSY_EVENTS_PORT", 3200))
    events = ESEvents(es_host=RECSY_EVENTS_URL, es_port=RECSY_EVENTS_PORT)


if not RECSY_SEARCH_KEY:
    raise (Exception("Specify env variable RECSY_SEARCH_KEY"))

port = int(config["app:main"].get("port", 5090))
no_milvus = config["app:main"].getboolean("no_milvus")
milvus_host = config["app:main"].get("milvus_host", "localhost")
milvus_db_path = config["app:main"].get("milvus_db_path", "/var/lib/milvus/db")
milvus_index_name = config["app:main"].get(
    "milvus_index_name", "europeana_prod_laser_300"
)


rec_algo = EuropeanaSearchRecAlgorithm(
    RECSY_SEARCH_KEY, RECSY_SEARCH_API
)  # ESRecAlgorithm()
es_rec_algo = ESRecAlgorithm()

eusearch = EuropeanaSearchRecAlgorithm(RECSY_SEARCH_KEY, RECSY_SEARCH_API).eusearch

milvus_rec_algo = None
if not no_milvus:
    milvus_rec_algo = MilvusAlgorithm(
        host=milvus_host,
        index=milvus_index_name,
        path_to_db=os.path.join(milvus_db_path, milvus_index_name),
        events=events,
    )

path_to_lmdbs = (
    "/opt/europeana_data/lmdbs/"  # TODO: remove as well as refactor /compare endpoint
)


def get_vector_from_embeddings_api(records: list):
    """
    Function returns the vector embeddings for the supplied records:

    Input should be the following:
    [
        {
            "id": string,
            "title": array(string),
            "description": array(string),
            "creator": array(string),
            "tags": array(string),
            "places": array(string),
            "times": array(string)
        }
    ]

    Return format: 
    
    {
        "data": [{"id": id, "embedding": [list of floats]}],
        "status": success|failure
    }
    """
    response = requests.post(
        f"{RECSY_EMBEDDINGS_API}/embedding_api/embeddings",
        json={"records": records},
        params={"reduce": 1},  # to get 300 dimensional vectors
    )

    return response.json()


def parse_jwt_token(token):
    parsed_data = jwt.decode(token, options={"verify_signature": False})
    return parsed_data


def get_items_from_bucket(
    bucket_id, token
):  # TODO: move this function out of main file and define endpoint in .env
    # endpoint = f"http://set-api-acceptance.eanadev.org/set/{bucket_id}?profile=standard"
    endpoint = f"{RECSY_USER_SETS_API}/{bucket_id}?profile=standard"
    header = {"Authorization": token}
    r = requests.get(endpoint, headers=header)
    if not r.ok:
        raise ServerError(
            f"Failed to retrieve bucket {bucket_id} content", status_code=r.status_code
        )
    res = r.json()

    def remove_prefix(text):
        prefix = "http://data.europeana.eu/item"
        return text[text.startswith(prefix) and len(prefix) :]

    return [remove_prefix(item) for item in res.get("items", [])]


app = Sanic(name="Europeana RecSy")
app.blueprint(swagger_blueprint)
app.config["API_SCHEMES"] = ["https"]
app.config.API_VERSION = "0.1"
app.config.API_TITLE = "Recommendation Engine API"
CORS(app)


def create_blueprint(name):
    prefix = "/"
    return Blueprint(f"{name}", url_prefix=f"{prefix}{name}")


bp_auth = create_blueprint("auth")
bp_bucket = Blueprint("buckets")
bp_events = create_blueprint("events")
# bp_task_bucket = Blueprint("buckets")
bp_search = create_blueprint("search")
bp_recommend = create_blueprint("recommend")


class User:
    user = str
    password = str


class Token:
    token = doc.String(description="JWT token received from /auth endpoint")


class Event:
    # user = doc.String(
    #     description="The identifier of the user as determined by the Authorization Server",
    #     required=True,
    # )
    session_id = doc.String(
        description="The identifier of the session as determined by the Authorization Server",
        required=True,
    )
    type = doc.String(
        description="The type of event, one of: `search`, `view`, `bucket_add`, `bucket_remove`, `favorites_add`, `favorites_remove`",
        required=True,
    )
    # bucket = doc.String(
    #     description="The identifier of the set as defined in the User Sets API",
    #     required=False,
    # )
    item = doc.String(
        description="The identifier of the item/record as defined in the Record API.",
        required=True,
    )
    # query = doc.String(description="The query", required=False)
    # qf = doc.String(description="Parameter of the source query", required=False)
    # sort = doc.String(description="Parameter of the source query", required=False)
    # rows = doc.Integer(description="Parameter of the source query", required=False)
    # start = doc.Integer(description="Parameter of the source query", required=False)


class Recommend:
    user = doc.String(
        description="The identifier of the user as determined by the Authorization Server",
        required=True,
    )
    item = doc.String(
        description="The identifier of the item/record as defined in the Record API",
        required=False,
    )
    bucket = doc.String(
        description="The identifier of the set as defined in the User Sets API",
        required=False,
    )


class Label:
    title = doc.String("Title of the record")
    description = doc.String("Description of the record")


TOKEN = "Authorization"


class AuthView(HTTPMethodView):
    @doc.consumes(User, location="body")
    @doc.produces(Token)
    @doc.summary("Authorize API with username and password. Returns token")
    def post(self, request):
        return json({"token": "dfgfhggdfg"})


class EventsView(HTTPMethodView):

    # post_param_description = """An array of JSON objects with the following model.
    #                 `user` (mandatory): the identifier of the user as determined by the Authorization Server.
    #                 `type` (mandatory): the type of sinal, one of: `search`, `view`, `bucket_add`, `bucket_remove`, `favorites_add`, `favorites_remove`
    #                 `bucket` (optional): the identifier of the set as defined in the User Sets API.
    #                 `item` (optional): the identifier of the item/record as defined in the Record API.
    #                 `query`, `qf`, `sort`, `rows`, `start` (all optional): the query and respective parameters."""

    post_param_description = """An array of JSON objects with the following model.
                     `session_id` (mandatory): the identifier of the user as determined by the Authorization Server.
                     `type` (mandatory): the type of signal, one of: `accept` or `reject`
                     `item` (optional): the identifier of the item/record as defined in the Record API."""

    @doc.summary("Add events")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(
        doc.List(Event, description=post_param_description, name="events"),
        location="body",
        required=False,  # seems to be a BUG - doesn't work with True here
    )
    def post(self, request):

        auth = request.headers.get(TOKEN)
        if auth is None:
            return text("Missing Authorization header\n", status=401)
        auth_fields = auth.split()
        if len(auth_fields) == 0 or auth_fields[0] not in ["Bearer", "APIKEY"]:
            abort(403, "Authorization header should contain either Bearer  or APIKEY")

        if RECSY_EVENTS_PROVIDER == "elasticsearch":
            mode = "es"
        elif RECSY_EVENTS_PROVIDER == "sql":
            mode = "sql"

        events_list = request.json  # getting list from the body

        valid_event_types = ["accept", "reject"]

        for event in events_list:
            session_id = event.get("session_id")
            type_of_event = event.get("type")

            # if user is None:  # which condition have to be here?
            #     return text("Unauthorized user\n", status=403)

            # checking if event type is valid
            if type_of_event not in valid_event_types or session_id is None:
                return text("Missing parameters\n", status=400)

            if mode == "elasticsearch":
                dict_update = {"_index": "events", "timestamp": datetime.now()}
                event.update(dict_update)

        if mode == "elasticsearch":
            res = events.save_events(events_list)
            if len(res) > 0:
                return empty(status=200)
            else:
                return text(res, status=500)
        else:
            events.save_events(events_list)
            return empty(status=200)

    @doc.summary("Get last 10 events")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(
        doc.String(description="Identifier of the session", name="session_id",),
        location="query",
    )
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    def get(self, request):
        auth = request.headers.get(TOKEN)
        if auth is None:
            return text("Missing Authorization header\n", status=401)
        auth_fields = auth.split()
        if len(auth_fields) == 0 or auth_fields[0] not in ["Bearer", "APIKEY"]:
            abort(403, "Authorization header should contain either Bearer  or APIKEY")

        session_id = request.args.get("session_id")
        res = events.get_last_events(last_n=10, session_id=session_id)
        return json(res)


# initial search: passed list of phrases/keywords -> returns items
class SearchView(HTTPMethodView):
    @doc.summary("Search by list of keywords")
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(
        doc.List(str, name="query", description="List of keywords to search by"),
        location="query",
        required=False,
    )
    @doc.consumes(
        doc.String(name="offset", description="Number of items to skip, default: 0"),
        location="query",
        required=False,
    )
    @doc.consumes(
        doc.String(name="limit", description="Number of items to return, default: 50"),
        location="query",
        required=False,
    )
    @doc.consumes(
        doc.String(name="source", description="Source of the search"),
        location="query",
        required=False,
    )
    def get(self, request):
        query = request.args.get("query")
        id = request.args.get("id")
        offset = int(request.args.get("offset", 0))
        limit = int(request.args.get("limit", 50))
        geolocated = bool(int(request.args.get("geolocated", False)))
        source = request.args.get("source")
        reuse = self._convert_reuse(request.args.getlist("reuse"))
        media_type = request.args.getlist("media-type")
        language = request.args.getlist("language")

        if source == "JHN":
            jhn_elastic_client = ESRecAlgorithm(
                es_host="localhost", es_port=9200, es_index="items"
            )
            if id:
                ids = id.split(",")
                results = jhn_elastic_client._get_records_from_ids(ids)
            else:
                results = jhn_elastic_client._multi_match_search(
                    query_text=query, limit=limit, offset=offset, geolocated=geolocated
                )

            results["count"] = len(results.get("results", []))
        else:
            if not query and not id:
                query = "*"
                # TODO: abort(400, "Missing mandatory query argument")
            if query is not None:
                results = dict()
                qf_geo = "pl_wgs84_pos_lat:*" if geolocated else None
                # TODO: extract to config file
                qf = [qf_geo] + self._convert_source(source)
                qf += self._convert_media_type(media_type)
                qf += self._convert_language(language)

                for q in ["title:" + query, query]:
                    q_results = eusearch.search(
                        q, qf=qf, rows=limit, start=offset + 1, reusability=reuse
                    )
                    if not results:
                        results = q_results
                        if results["count"] >= limit:
                            break
                    else:
                        for key in ["results", "count", "total_count"]:
                            results[key] += q_results[key]
            elif id:
                ids = id.split(",")
                results = eusearch.search_ids(ids)

        results["offset"] = offset
        return json(results, escape_forward_slashes=False, ensure_ascii=False)

    def _convert_reuse(self, reuse):
        if not reuse:
            return None
        map = {
            "yes": "open",
            "yes with conditions": "restricted",
            "permission required": "permission",
        }
        eu_reusability = []
        for value in reuse:
            if value in map:
                eu_reusability.append(map[value])
        return ",".join(eu_reusability) if eu_reusability else None

    def _convert_source(self, source):
        sources = []
        if source == "Grodzka":
            sources.append('DATA_PROVIDER:"Ośrodek \\"Brama Grodzka \\- Teatr NN\\""')
        elif source in ["Europeana", "EuropeanaXX"]:
            pass  # default = all Europeana data is available
        elif source == "JHN-Europeana":
            sources.append('PROVIDER:"Judaica Europeana\\/Jewish Heritage Network"')
        elif source == "JHM":
            sources.append('DATA_PROVIDER:"Jewish Historical Museum"')
            # sources.append('DATA_PROVIDER:"Joods Historisch Museum"')
        return sources

    def _convert_media_type(self, media_type):
        if not media_type:
            return []

        types = ["text", "image", "video", "sound", "3d"]
        eu_media = []
        for value in media_type:
            if value in types:
                eu_media.append(f'TYPE:"{value.upper()}"')
        return eu_media

    def _convert_language(self, language):
        if not language:
            return []

        eu_langs = []
        for value in language:
            # Try to convert by name or by iso639-2 code
            if len(value) == 2:
                try:
                    lang_obj = languages.get(alpha2=value.lower())
                except:
                    pass
            else:
                lang_obj = languages.name.get(value.capitalize())
            if lang_obj:
                eu_langs.append(f'LANGUAGE:"{lang_obj.alpha2}"')
        return eu_langs


class RecommendView(HTTPMethodView):
    # @bp_items.get(
    #     "/<item_id:[\w/-]*>/recommend"
    # )  # for `/` support: https://github.com/huge-success/sanic/issues/1473#issuecomment-455020740
    # @doc.summary("Recommend items to either item or bucket via ES")
    # @doc.description(
    #     "Recommendation is based either on `item` or `bucket`. Hence one of these fields have to be indicated."
    # )
    # @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    # @doc.consumes(Recommend, location="body", required=True)
    # def post(self, request):
    #     item = request.json.get("item")
    #     bucket = request.json.get("bucket")
    #     # user = request.json.get("user")
    #     if bucket is None and item is None:
    #         return text("Please indicate either `bucket` or `item`")
    #     result = es_rec_algo.recommend([item], limit=10)
    #     return json(result)

    @bp_recommend.post("/compare")
    @doc.summary("Compare recommendations by different algorithms")
    @doc.description(
        "Recommendation is based either on `item` or `bucket`. Hence one of these fields have to be indicated."
    )
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(
        doc.List(
            name="Algorithm",
            choices=[
                "bpemb",
                "sentencetransformers",
                "Janna_API",
                "Europeana_API",
                "Elastic",
            ],
        ),
        location="body",
        required=True,
    )
    @doc.consumes(Recommend, location="body", required=True)
    def post_compare(self, request):
        item = request.json.get("item")
        bucket = request.json.get("bucket")
        algorithms = request.json.get("Algorithm")
        # user = request.json.get("user")
        if bucket is None and item is None:
            return text("Please indicate either `bucket` or `item`")

        result = {}
        for alg in algorithms:
            if alg in ["bpemb", "sentencetransformers", "Janna_API"]:
                recommender = MilvusAlgorithm(path_to_db=path_to_lmdbs + alg)
            elif alg == "Elastic":
                recommender = es_rec_algo
            else:
                recommender = rec_algo

            res = recommender.recommend([item], limit=10)
            result.update({alg: res})

        # result = es_rec_algo.recommend([item], limit=10)
        return json(result)

    @doc.summary("Recommend items to either item or bucket")
    @doc.description(
        "Recommendation is based either on `item` or `bucket`. Hence one of these fields have to be indicated."
    )
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(
        doc.String(
            description="The identifier of the item/record as defined in the Record API",
            name="item",
        ),
        location="query",
    )
    @doc.consumes(
        doc.String(
            description="The identifier of the set as defined in the User Sets API",
            name="bucket",
        ),
        location="query",
    )
    @doc.consumes(
        doc.Integer(
            name="size",
            description="the number of recommendations to be returned (limited to at most 50), by default set to 10.",
        ),
        location="query",
    )
    @doc.consumes(
        doc.Integer(
            name="skip",
            description="the number of recommendations to be skipped for pagination (limited to at most 40), by default set to 0.",
        ),
        location="query",
    )
    @doc.consumes(
        doc.String(
            name="session_id",
            description="unique session id (if `client_public_id` is not defined inside token)",
        ),
        location="query",
    )
    @doc.consumes(
        doc.Integer(name="seed", description="random seed of the recommendations",),
        location="query",
    )
    def get(self, request):
        auth = request.headers.get(TOKEN, "")
        auth_fields = auth.split()
        if len(auth_fields) == 0 or auth_fields[0] not in ["Bearer", "APIKEY"]:
            abort(403, "Authorization header should contain either Bearer  or APIKEY")

        if auth_fields[0] == "Bearer":
            try:
                parsed_token = parse_jwt_token(auth_fields[1])
                session_id = parsed_token.get("client_public_id")
            except:
                session_id = None

            if not session_id:
                abort(403, "Please check your Bearer token")
        else:
            session_id = request.args.get("session_id")

        if not session_id:
            abort(403, "Session id should be provided as a part of token or directly")

        item = request.args.get("item")
        bucket = request.args.get("bucket")
        size = int(request.args.get("size", 10))
        skip = int(request.args.get("skip", 0))
        seed = int(request.args.get("seed", 1))
        source = request.args.get("source")

        if source == "JHN":
            jhn_elastic_client = ESRecAlgorithm(
                es_host="localhost", es_port=9200, es_index="items"
            )

        if bucket is None and item is None:
            abort(
                400,
                "Missing or conflicting parameters. Either item or bucket should be indicated.",
            )

        if item is not None:
            # First, try recommendation from Milvus, then fallback to default (Europeana)
            result = (
                []
                if no_milvus
                else milvus_rec_algo.recommend(
                    item, limit=size, offset=skip, session_id=session_id
                )
            )
            if not result:
                if source == "JHN":
                    result = jhn_elastic_client.recommend(item, limit=size, offset=skip)
                else:
                    result = rec_algo.recommend(
                        item, limit=size, offset=skip,
                    )  # rec_algo.recommend("/91627/SMVK_EM_fotografi_2467512", limit=size)
        else:
            if auth is None:
                abort(401, "Missing Authorization header\n")

        if bucket is not None:
            # 1. get items from europeana API
            items_list = get_items_from_bucket(bucket, auth)
            if not items_list:
                logging.warning(f"Bucket {bucket} is empty")
            # 2 try recommendation from Milvus, then fallback to default (Europeana)
            result = (
                milvus_rec_algo.recommend(
                    items_list, limit=size, offset=skip, session_id=session_id
                )
                if milvus_rec_algo
                else []
            )
            # 3. get recomendations based on item list
            if not result:
                if source == "JHN":
                    result = jhn_elastic_client.recommend(
                        items_list, limit=size, offset=skip
                    )
                else:
                    result = rec_algo.recommend(items_list, limit=size, offset=skip)

            # result = rec_algo.recommend("/91627/SMVK_EM_fotografi_2467512", limit=size)
        if result is None:  # item or bucket id is not found
            abort(
                404,
                "Item or bucket {} is not found or empty\n".format(
                    item if item else bucket
                ),
            )

        random.Random(seed).shuffle(result)
        return json(result, escape_forward_slashes=False)

    @bp_recommend.post("/entity")
    @doc.summary("Recommend for entity")
    @doc.description(
        """Recommendation is based on title, descriptions and items from the provided body.
        Structure of the body:
        
        {
            "labels": [ {"title": title1, "description": description1}, {"title": titleN, "description": descriptionN}],
            "items": [ "itemID1", "itemIDN" ]
        }
        """
    )
    @doc.consumes(doc.String(name=TOKEN), location="header", required=True)
    @doc.consumes(
        doc.Integer(
            name="size",
            description="the number of recommendations to be returned (limited to at most 50), by default set to 10.",
        ),
        location="query",
    )
    @doc.consumes(
        doc.String(
            name="session_id",
            description="unique session id (if `client_public_id` is not defined inside token)",
        ),
        location="query",
    )
    @doc.consumes(
        doc.JsonBody({"labels": doc.List(Label), "items": doc.List(doc.String())}),
        location="body",
    )
    def recommend_to_entity(request):
        auth = request.headers.get(TOKEN, "")
        auth_fields = auth.split()
        if len(auth_fields) == 0 or auth_fields[0] not in ["Bearer", "APIKEY"]:
            abort(403, "Authorization header should contain either Bearer  or APIKEY")

        if auth_fields[0] == "Bearer":
            try:
                parsed_token = parse_jwt_token(auth_fields[1])
                session_id = parsed_token.get("client_public_id")
            except:
                session_id = None

            if not session_id:
                abort(403, "Please check your Bearer token")
        else:
            session_id = request.args.get("session_id")

        if not session_id:
            abort(403, "Session id should be provided as a part of token or directly")

        records = request.json
        size = int(request.args.get("size", 10))

        items_ids = records.get("items")
        records_to_find = []
        for n, record in enumerate(records.get("labels")):
            records_to_find.append(
                {
                    "id": f"{session_id}_{n}",
                    "title": [record.get("title", "")],
                    "description": [record.get("description", "")],
                }
            )

        vectors = get_vector_from_embeddings_api(records_to_find)["data"]
        vectors = [i["embedding"] for i in vectors]
        # getting milvus results
        result = (
            []
            if no_milvus
            else milvus_rec_algo.recommend(
                items_ids, limit=size, offset=0, session_id=session_id, vectors=vectors
            )
        )

        if not result:  # if no milvus results -> fallback to default (Europeana)
            result = rec_algo.recommend(items_ids, limit=size)

        return json(result, escape_forward_slashes=False)

    @bp_recommend.get("/status")
    @doc.summary("Get service status")
    def get_status(request):
        request.headers[
            TOKEN
        ] = "APIKEY status_test_key"  # Bearer cannot be used here cause it's validated in requests now
        request.args["session_id"] = "status_request"
        request.args["item"] = ["/317/04ee3e2b_0f24_4d29_9f29_29712a0e68ae"]
        return RecommendView().get(request)


def add_blueprint(bp, view):
    bp.add_route(view.as_view(), "/")
    app.blueprint(bp)


add_blueprint(bp_auth, AuthView)
# add_blueprint(bp_user, UserView)
# add_blueprint(bp_task, TaskView)
add_blueprint(bp_events, EventsView)
# add_blueprint(bp_bucket, BucketView)
add_blueprint(bp_search, SearchView)
add_blueprint(bp_recommend, RecommendView)


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s:%(levelname)s:%(message)s", level=logging.DEBUG
    )

    # from pathlib import Path

    # parser = argparse.ArgumentParser(description="Indicate settings for app: ")
    # parser.add_argument(
    #     "-p", "--port", type=int, default=8000, help="indicate port for application"
    # )
    # parser.add_argument(
    #    "--no_milvus", action="store_true", help="Do not use Milvus"
    # )
    # parser.add_argument(
    #    "--milvus_host", type=str, default="localhost", help="Milvus host IP"
    # )
    # parser.add_argument(
    #     "-d", "--milvus_db_path", type=str, default=os.path.join(str(Path.home()), "milvus", "db"), help="Path to Milvus database" )
    # parser.add_argument(
    #     "-i", "--milvus_index_name", type=str, default="europeana_prod_laser_300", help="Milvus index name"
    # )

    # args = parser.parse_args()
    # port = args.port

    # milvus_rec_algo = None
    # if not args.no_milvus:
    #     milvus_rec_algo = MilvusAlgorithm(host=args.milvus_host, index=args.milvus_index_name,
    #                                   path_to_db=os.path.join(args.milvus_db_path,args.milvus_index_name ))

    workers = int(os.getenv("RECSY_NUM_WORKERS", cpu_count()))

    app.config.API_VERSION = "0.1"
    app.config.API_TITLE = "Recommendation Engine API"
    app.run(host="0.0.0.0", port=port, workers=workers)
