import os
import time
import argparse
from typing import List
from elasticsearch import Elasticsearch
from zipfile import ZipFile
from ftp_loader import FTPLoader
from zip_reader import process_zip_list
from elastic_indexer import parse_dir, index_to_es


def process_files(files_list: List, mode="es"):
    """Function process list of files, which is by convention 100 filenames of zips, which have to be loaded from FTP and processed
    
    :mode: - es (ElasticSearch) or (embeds) for embeddings creation
    
    """
    start = time.time()
    # Set variables depending on the mode
    if mode == "es":
        path_to_processed_zips = "parsed_records/zips_each_10/"
        zip_download_folder = "zip_archive/"
        path_to_write_records = "parsed_records/"
    elif mode == "full":
        path_to_processed_zips = "parsed_records/zips/"
        zip_download_folder = "zip_archive/"
        path_to_write_records = "parsed_records/"

    # CHECK IF FILES ARE ALREADY PROCESSED

    print("------------- Checking already processed files -------------")
    processed_files = [
        f for f in os.listdir(path_to_processed_zips) if f.endswith(".zip")
    ]
    c = 0
    to_remove_from_list = []
    for fname in files_list:
        if fname in processed_files:
            to_remove_from_list.append(fname)
            c += 1

    files_list = [x for x in files_list if x not in to_remove_from_list]
    if c > 0:
        print(f"{c} files out of {BATCH_SIZE} were already processed!")

    print("------------- Loading files from FTP -------------")
    written_dict = ftp_client.download_files(files_list, savedir=zip_download_folder)
    if len(written_dict["errors"]) > 0:
        print(
            f"Errors occured for {len(written_dict['errors'])} files out of {len(files_list)}"
        )

    print("------------- Parsing XML data -------------")
    if mode == "es":
        process_zip_list(
            written_dict["success"],
            zip_download_folder,
            path_to_write_records,
            each_10=True,
        )
    else:
        process_zip_list(
            written_dict["success"],
            zip_download_folder,
            path_to_write_records,
            each_10=False,
        )

    if mode == "es":
        print("------------- Indexing to ES -------------")
        es = Elasticsearch([{"host": "localhost", "port": 3200}])
        index = "europeana_data_v3"

        for i, input_fn in enumerate(parse_dir(path_to_write_records)):
            index_to_es(es, index, path_to_write_records + input_fn)

        es_indexed = es.count(index=index).get("count")
        print(f"Total indexed documents in index {index} is: {es_indexed}")

    print("------------- Removing processed zips -------------")
    deleted_c = 0
    for fname in files_list:
        if os.path.isfile(os.path.join(zip_download_folder, fname)):
            os.remove(os.path.join(zip_download_folder, fname))
            deleted_c += 1
    print(f"{deleted_c} files were deleted from {zip_download_folder} folder")
    print()
    print("------------- Zipping processed jsons -------------")
    prefixes_for_zip = []
    for fname in os.listdir(path_to_write_records):
        filename = os.fsdecode(fname)
        if filename.endswith(".json"):
            f = filename.split("_")[0]
            prefixes_for_zip.append(f)

    prefixes_for_zip = list(set(prefixes_for_zip))
    for prefix in prefixes_for_zip:
        files_to_zip = []
        for fname in os.listdir(path_to_write_records):
            filename = os.fsdecode(fname)
            if filename.endswith(".json") and filename.startswith(prefix):
                files_to_zip.append(path_to_write_records + filename)
        with ZipFile(path_to_processed_zips + prefix + ".zip", "w") as zipObj:
            for file_to_zip in files_to_zip:
                zipObj.write(
                    file_to_zip,
                    prefix + "/" + file_to_zip.split(path_to_write_records)[1],
                )
                os.remove(os.path.join(file_to_zip))

    print()
    print(
        f"Total time for processing {len(files_list)} zips took: {round(time.time() - start, 2)} seconds ({round((time.time() - start)/60, 2)} minutes)."
    )
    print()
    print("-" * 33)
    print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Select mode of operation")
    parser.add_argument(
        "-m",
        "--mode",
        dest="mode",
        type=str,
        help="select mode (`es` for ElasticSearch and 10 pct of data or `full`)",
    )

    args = parser.parse_args()

    mode = str(args.mode)

    BATCH_SIZE = 100
    ftp_client = FTPLoader(ftp_url="download.europeana.eu", working_dir="dataset")
    filenames = ftp_client.list_files()

    files_to_process = []
    counter = 0
    for f in filenames:
        if f.endswith(".zip"):
            files_to_process.append(f)
            if len(files_to_process) >= BATCH_SIZE:
                counter += 1
                print(f"Processing batch No. {counter}")
                process_files(files_to_process, mode)
                files_to_process = []

    if len(files_to_process) > 0:
        process_files(files_to_process, mode)
