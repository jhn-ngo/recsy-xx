import os
import zipfile
import tqdm
import json
from lxml import etree  # need this to support `tag`


# VARIABLES FOR XML PARSING
NS_ORE_PROXY = "{http://www.openarchives.org/ore/terms/}Proxy"
EUROPEANA_PROXY = "{http://www.europeana.eu/schemas/edm/}europeanaProxy"

tag_dc_title = "{http://purl.org/dc/elements/1.1/}title"
tag_dc_description = "{http://purl.org/dc/elements/1.1/}description"
tag_dc_creator = "{http://purl.org/dc/elements/1.1/}creator"
tag_dc_subject = "{http://purl.org/dc/elements/1.1/}subject"
tag_dc_spatial = "{http://purl.org/dc/terms/}spatial"
tag_dc_temporal = "{http://purl.org/dc/terms/}temporal"

tag_mapping = {
    "title": tag_dc_title,
    "description": tag_dc_description,
    "creator": tag_dc_creator,
    "tags": tag_dc_subject,
    "places": tag_dc_spatial,
    "times": tag_dc_temporal,
}


def extract_properites(segment, filename):
    """Function parses segment and returns record"""
    record = {
        "id": filename,
        "title": [],  # dctitle
        "description": [],  # dcDescription
        "creator": [],  # dcCreator
        "tags": [],  # dcsubject
        "places": [],  # dcspatial (some place)
        "times": [],  # dctemporal
    }

    for key, tag in tag_mapping.items():
        record[key] = [a.text for a in segment.findall(tag) if a.text not in ["", None]]

    return record


def parse_zip(path_to_zip, each_10=False):
    print(f"Parsing {path_to_zip}")
    fname, fext = os.path.splitext(path_to_zip)
    if fext == ".zip":
        try:
            zip = zipfile.ZipFile(path_to_zip, "r")
        except:
            print(f"ZIP {path_to_zip} cannot be opened!")
            return None
        tmx_fnames = zip.namelist()
    else:
        zip = None
        tmx_fnames = [path_to_zip]

    s = 0
    for tmx_fname in tqdm.tqdm(tmx_fnames, desc="XML records"):
        tmx_fname_base = os.path.basename(tmx_fname)  #
        if tmx_fname_base:  # check if this is not a directory

            tmx_file = zip.open(tmx_fname) if zip else open(tmx_fname, mode="rb")
            context = etree.iterparse(tmx_file, events=("start",), tag=[NS_ORE_PROXY],)
            try:
                i = 0

                for segment in iter(
                    context
                ):  # If found any invalid part in xml, stop the process
                    # if i % 100 == 0:
                    #      print(f"Parsed {i} segments")
                    #      print(f"Sample segment: {segment}")
                    i += 1
                    if len(segment[1].findall(EUROPEANA_PROXY)) > 0:
                        is_europeana_proxy = segment[1].findall(EUROPEANA_PROXY)[0].text
                        if (
                            is_europeana_proxy == "false"
                        ):  # check that it's no europeana proxy
                            s += 1  # to take 10% of records..
                            if each_10:
                                if s % 10 == 0:
                                    yield segment, tmx_fname
                            else:
                                yield segment, tmx_fname
            except etree.XMLSyntaxError:  # check if file is well formed
                print(f"Skipping invalid XML {tmx_fname}")


def write_records(folder_to_write: str, batch_name: str, records: list):
    with open(folder_to_write + batch_name + ".json", "w") as json_file:
        json.dump(records, json_file)


def list_zips(path_to_zip_folder: str):
    ziplist = []
    if os.path.exists(path_to_zip_folder):
        # list all zips inside
        ziplist = [
            os.path.join(path_to_zip_folder, f)
            for f in os.listdir(path_to_zip_folder)
            if os.path.isfile(os.path.join(path_to_zip_folder, f))
        ]
        ziplist = [i for i in ziplist if i.endswith(".zip")]
        print(f"Total amount of zips to process: {len(ziplist)}")
    else:
        print(f"There is no directory: {path_to_zip_folder}")

    return ziplist


def process_zips(path_to_zip_folder, path_to_write_records):
    BATCH_SIZE = 100

    if not os.path.exists(path_to_write_records):
        os.makedirs(path_to_write_records)

    zipfiles = list_zips(path_to_zip_folder)
    if len(zipfiles) == 0:
        return None

    records = []
    for zip_file in zipfiles:
        batch_counter = 0
        batch_name = (
            os.path.splitext(zip_file.split(path_to_zip_folder)[1])[0]
            + "_"
            + str(batch_counter)
        )
        for segment, filename in parse_zip(zip_file):
            record_id = (
                os.path.splitext(zip_file.split(path_to_zip_folder)[1])[0]
                + "/"
                + os.path.splitext(filename)[0]
            )
            record = extract_properites(segment[1], record_id)
            records.append(record)
            if len(records) >= BATCH_SIZE:
                write_records(path_to_write_records, batch_name, records)
                records = []
                batch_counter += 1
                batch_name = (
                    os.path.splitext(zip_file.split(path_to_zip_folder)[1])[0]
                    + "_"
                    + str(batch_counter)
                )

        if len(records) > 0:
            write_records(path_to_write_records, batch_name, records)


def process_zip_list(
    zipfiles, path_to_zip_folder, path_to_write_records, each_10=False
):
    BATCH_SIZE = 100

    if not os.path.exists(path_to_write_records):
        os.makedirs(path_to_write_records)

    if len(zipfiles) == 0:
        print("There is no zipfiles! Exiting!!!")
        return None

    zipfiles = [
        os.path.join(path_to_zip_folder, f)
        for f in zipfiles
        if os.path.isfile(os.path.join(path_to_zip_folder, f))
    ]

    records = []
    for zip_file in zipfiles:
        batch_counter = 0
        batch_name = (
            os.path.splitext(zip_file.split(path_to_zip_folder)[1])[0]
            + "_"
            + str(batch_counter)
        )
        for segment, filename in parse_zip(zip_file, each_10=each_10):
            record_id = (
                os.path.splitext(zip_file.split(path_to_zip_folder)[1])[0]
                + "/"
                + os.path.splitext(filename)[0]
            )
            record = extract_properites(segment[1], record_id)
            records.append(record)
            if len(records) >= BATCH_SIZE:
                write_records(path_to_write_records, batch_name, records)
                records = []
                batch_counter += 1
                batch_name = (
                    os.path.splitext(zip_file.split(path_to_zip_folder)[1])[0]
                    + "_"
                    + str(batch_counter)
                )

        if len(records) > 0:
            write_records(path_to_write_records, batch_name, records)


if __name__ == "__main__":
    # HERE IS EXAMPLE OF USAGE
    path_to_zip_folder = "zip_archive/"
    path_to_write_records = "parsed_records/"
    process_zips(path_to_zip_folder, path_to_write_records)
