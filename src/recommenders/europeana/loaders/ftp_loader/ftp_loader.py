import os
from typing import List
from ftplib import FTP


class FTPLoader:
    """Class for connecting to FTP and loading files"""

    def __init__(self, ftp_url: str, working_dir: str = ""):
        self.ftp_url = ftp_url
        self.working_dir = working_dir
        self.ftp = FTP(self.ftp_url)
        self.ftp.login()
        if working_dir:
            self.ftp.cwd(working_dir)

    def list_files(self):
        filenames = self.ftp.nlst()
        filenames = [f for f in filenames if f.endswith(".zip")]
        print(
            f"Total number of files in {self.working_dir} directory is: {len(filenames)}"
        )
        return filenames

    def download_files(self, filenames: List, savedir: str):
        written_dict = {"success": [], "errors": []}

        if not os.path.exists(savedir):
            os.makedirs(savedir)

        for fn in filenames:
            if fn.endswith(".zip"):
                if os.path.exists(savedir + fn):
                    written_dict["success"].append(fn)
                    continue

                try:
                    with open(savedir + fn, "wb") as file:
                        self.ftp.retrbinary(f"RETR {fn}", file.write)
                    written_dict["success"].append(fn)
                except:
                    written_dict["errors"].append(fn)

        return written_dict


if __name__ == "__main__":
    ftp_client = FTPLoader(ftp_url="download.europeana.eu", working_dir="dataset")
    filenames = ftp_client.list_files()
    written_dict = ftp_client.download_files(filenames, "zip_archive/")
    print(written_dict)
