from datetime import datetime
from elasticsearch import Elasticsearch, helpers
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, TIMESTAMP
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


db_connection_string = "postgresql+psycopg2://root:root@localhost/events"
Base = declarative_base()


class ESEvents:
    def __init__(self, es_host="209.182.239.246", es_port=3200):
        self.host = es_host
        self.port = es_port
        self.client = Elasticsearch([{"host": self.host, "port": self.port}])

        # creating events index if it doesn't exists
        # ! it brokes with gunicorn run
        # if not self.client.indices.exists(index="events"):
        #     self.client.indices.create(index='events')

    def save_event(self, event_type: str, user_id: str, **params):
        event_doc = {
            "event_type": event_type,
            "timestamp": datetime.now(),  # .strftime("%Y-%m-%d %H:%M:%S"
            "user_id": user_id,
        }
        for key, value in params.items():
            if value is not None:
                event_doc.update({key: value})
        response = self.client.index(index="events", body=event_doc)

        return response

    def save_events(self, events_list):
        response = helpers.bulk(self.client, events_list)
        return response

    def get_event(self, doc_id):
        response = self.client.get(index="events", id=doc_id)
        return response

    def get_last_events(self, last_n: int = 10, session_id=None):
        if not session_id:
            response = self.client.search(
                index="events", size=last_n, sort="timestamp:desc"
            )
        else:
            response = self.client.search(
                index="events",
                size=last_n,
                sort="timestamp:desc",
                body={"query": {"match": {"session_id": session_id}}},
            )

        res = response["hits"]["hits"]
        res = [i["_source"] for i in res]
        return res


class Event_Table(Base):
    __tablename__ = "events"
    id = Column(Integer, primary_key=True)
    session_id = Column("session_id", String)
    item_id = Column("item", String)
    event_type = Column("type", String)
    timestamp = Column(
        "timestamp", TIMESTAMP(timezone=False), nullable=False, default=datetime.now()
    )

    def __init__(self, session_id, item_id, event_type):
        self.session_id = session_id
        self.item_id = item_id
        self.event_type = event_type


class SQLEvents:
    def __init__(self, db_connection_string, events=Event_Table) -> None:
        # pool_pre_ping added as workaround for issue described below:
        # https://docs.sqlalchemy.org/en/14/core/pooling.html#disconnect-handling-pessimistic
        self.engine = create_engine(db_connection_string, pool_pre_ping=True) 
        Base.metadata.create_all(self.engine)  # it will override exisiting table
        self.Session = sessionmaker(bind=self.engine)
        # case when table is not created yet
        self.events = events

    def save_event(self, session_id: str, item_id: str, event_type: str):
        self.session = self.Session()
        event = self.events(
            session_id=session_id, item_id=item_id, event_type=event_type
        )
        self.session.add(event)
        self.session.commit()
        self.session.close()

    def save_events(self, events_list):
        self.session = self.Session()
        events_to_save = [
            self.events(
                session_id=i["session_id"], item_id=i["item"], event_type=i["type"],
            )
            for i in events_list
        ]
        self.session.bulk_save_objects(events_to_save)
        self.session.commit()
        self.session.close()

    def get_last_events(self, last_n: int = 10, session_id=None):
        self.session = self.Session()


        if session_id:
            last_events = (
                self.session.query(self.events)
                .filter(self.events.session_id == session_id)
                .order_by(self.events.timestamp.desc())
                .limit(last_n)
                .all()
            )
        else:
            last_events = (
                self.session.query(self.events)
                .order_by(self.events.timestamp.desc())
                .limit(last_n)
                .all()
            )

        self.session.commit()
        self.session.close()

        return [
            {
                "session_id": i.session_id,
                "item": i.item_id,
                "type": i.event_type,
                "timestamp": i.timestamp.isoformat(),
            }
            for i in last_events
        ]

if __name__ == "__main__":
    events = SQLEvents("postgresql+psycopg2://recsy:recsy@localhost/events")
    for i in range(0, 1000):
        print(events.get_last_events())
