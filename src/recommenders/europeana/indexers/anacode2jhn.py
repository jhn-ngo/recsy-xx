import os
import tqdm
import shutil
import numpy as np

def convert(keys_dir, embeds_dir, out_dir):
    for dir_w_encoded_records in tqdm.tqdm(sorted(os.listdir(keys_dir)), desc="next directory"):
        print(f"Processing dataset {dir_w_encoded_records}")
        keys_fn = os.path.join(keys_dir, dir_w_encoded_records)
        embeds_fn = os.path.join(embeds_dir, dir_w_encoded_records)

        keys_files = sorted(os.listdir(keys_fn))
        embeds_files = sorted(os.listdir(embeds_fn))

        out_data_dir = os.path.join(out_dir, dir_w_encoded_records)
        os.makedirs(out_data_dir)

        # Write output
        with open(os.path.join(out_data_dir, "keys.txt"), "w") as out_keys_f:
            with open(os.path.join(out_data_dir, "embeds_nn.bin"), "wb") as out_embeds_f:
                for keys_file, embeds_file in zip(keys_files, embeds_files):
                    vectors = np.load(embeds_file)
                    with open(keys_file, 'r') as keys_f:
                        keys = keys_f.readlines()
                    assert(len(keys) == len(vectors))

                    out_keys_f.writelines(keys)
                    for vector in vectors:
                        np.save(out_embeds_f, vector)
        print("Removing original embeddings")
        os.remove(embeds_files)

if __name__ == "__main__":
    import sys
    convert(sys.argv[0],sys.argv[1], sys.argv[2])