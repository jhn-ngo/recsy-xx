import os
import csv
import argparse
import tqdm
import numpy
import lmdb
import hnswlib

class FileRecordIterator:
    def __init__(self, path):
        self.path = path

    def __call__(self):
        for dir_w_encoded_records in tqdm.tqdm(sorted(os.listdir(self.path)), desc="next directory"):
            keys_fn = os.path.join(self.path, dir_w_encoded_records, "keys.txt")
            embeds_fn = os.path.join(self.path, dir_w_encoded_records, "embeds_nn.bin")
            if os.path.isfile(keys_fn) and os.path.isfile(embeds_fn):
                yield (keys_fn, embeds_fn, dir_w_encoded_records)


class HnswIndexer:
    def __init__(self, table_name):
        self.engine = None 
        self.table_name = table_name
    def index(self, vectors_fn, keys_fn, dataset):
        # constants:
        BATCH_SIZE = 10000

        fvectors = open(vectors_fn, "rb")
        fkeys = open(keys_fn, "r")

        ids = []
        vectors = []

        if True: #with self.id2key_db.begin(write=True) as txn_id2key:
           if True:  #with self.key2id_db.begin(write=True) as txn_key2id:
                for key in tqdm.tqdm(fkeys, desc=f"reading {self.table_name} keys"):
                    vector = numpy.load(fvectors)
                    vectors.append(vector.tolist())
                    # Create engine while detecting dimension
                    if not self.engine:
                        #pass
                        dimension = len(vectors[0])
                        self._init_engine(self.table_name, dimension)

                    key_val = key.replace("\n", "").encode()[:510]
                    id_val = b'%d' % self.i
                   
                    try:
                       pass
                       #txn_id2key.put(id_val, key_val, dupdata=False, overwrite=False)
                       #txn_key2id.put(key_val, id_val, dupdata=False, overwrite=False)
                    except:
                       print(f"Failed to index in lmdb, key: {key_val}, id: {id_val}")

                    ids.append(self.i)
                    self.i += 1
                    if len(ids) >= BATCH_SIZE:
                        #self.engine.index(vectors, ids, partition=dataset)
                        self.engine.add_items(vectors, ids)
                        ids = []
                        vectors = []
                        print(f"Indexed {self.i} vectors")

                if len(ids):
                    #self.engine.index(vectors, ids, partition=dataset)
                    self.engine.add_items(vectors, ids)


        fvectors.close()
        fkeys.close()

    def _init_engine(self, table, dimension):
        print(f"Creating table: {table} with dimension {dimension}")
        self.engine = hnswlib.Index(space='l2', dim=dimension)
        self.engine.init_index(max_elements=55000000, ef_construction=200, M=16)

def parse_args():
    parser = argparse.ArgumentParser(description="Index directory with keys and bin files to milvus")
    parser.add_argument(
        "--input", "-i",
        help="Input directory with encoded files inside other dirs (each folder should contain keys.txt and embeds_nn.bin)",
        required=True
    )
    parser.add_argument(
        "--table",
        "-t",
        help="Table name to index records inside milvus",
        type = str,
        required=True

    )


    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    files_iter = FileRecordIterator(args.input)
    indexer = HnswIndexer(args.table)
    for keys_fn, embeds_fn, dataset in files_iter():
        indexer.index(embeds_fn, keys_fn, dataset)
