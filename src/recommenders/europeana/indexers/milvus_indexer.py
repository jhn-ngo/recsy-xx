import logging
import os
import csv
import argparse
import tqdm
import numpy
import lmdb
import tempfile
import boto3
from pathlib import Path

from collections import OrderedDict

from engines.searchers.engine import SearchEngine
from milvus import IndexType

INDEX_TYPE = {
    "IVF_SQ8": {"type": IndexType.IVF_SQ8, "params": {"nlist": 16384}},
    "IVF_PQ": {"type": IndexType.IVF_PQ, "params": {"nlist": 16384, "m": 12}},
    "HNSW": {"type": IndexType.HNSW, "params": {"efConstruction": 10, "M": 48}},
}


class FileRecordIterator:
    def __init__(self, path):
        self.path = path

    def __call__(self):
        for dir_w_encoded_records in tqdm.tqdm(
            sorted(os.listdir(self.path)), desc="next directory"
        ):
            keys_fn = os.path.join(self.path, dir_w_encoded_records, "keys.txt")
            embeds_fn = os.path.join(self.path, dir_w_encoded_records, "embeds_nn.bin")
            if os.path.isfile(keys_fn) and os.path.isfile(embeds_fn):
                yield (keys_fn, embeds_fn, dir_w_encoded_records)


class S3FileRecordIterator:
    def __init__(self, bucket, prefix, only_keys=False):
        self.bucket = bucket
        self.prefix = prefix
        self.tmpdir = tempfile.gettempdir()
        self.only_keys = only_keys

    def __call__(self):
        self.aws_session = boto3.Session(profile_name="wasabi")
        self.client = self.aws_session.client(
            "s3", endpoint_url="https://s3.eu-central-1.wasabisys.com"
        )
        paginator = self.client.get_paginator("list_objects_v2")
        page_iterator = paginator.paginate(Bucket=self.bucket, Prefix=self.prefix)
        datasets = OrderedDict()
        for page in page_iterator:
            for s3_record in page["Contents"]:
                _, dataset_id, file_name = s3_record["Key"].split("/")
                if not dataset_id in datasets:
                    datasets[dataset_id] = OrderedDict()
                datasets[dataset_id][file_name] = s3_record["Key"]

        for dataset_id, dataset_files in tqdm.tqdm(datasets.items()):
            logging.debug(f"Downloading only keys for {dataset_id}")
            output = list()
            for file_name in ["keys.txt", "embeds_nn.bin"]:
                output.append(os.path.join(self.tmpdir, file_name))
                if not self.only_keys or file_name == "keys.txt":
                    logging.debug(
                        f"Downloading file from S3: {dataset_files[file_name]}"
                    )
                    self.client.download_file(
                        self.bucket, dataset_files[file_name], output[-1]
                    )
                else:
                    logging.debug(f"Skipping file from S3: {dataset_files[file_name]}")
                    Path(output[-1]).touch()
            output.append(dataset_id)
            yield output


class MilvusIndexer:
    def __init__(self, table_name, index_type):
        self.table_name = table_name
        self.index_type = index_type
        self.i = 0

        self.engine = None

        lmdb_map_size = int(5e10)
        self.id2key_db = lmdb.Environment(
            table_name + "_id2key", map_size=lmdb_map_size
        )
        self.key2id_db = lmdb.Environment(
            table_name + "_key2id", map_size=lmdb_map_size
        )

    def index(self, vectors_fn, keys_fn, dataset):
        # constants:
        BATCH_SIZE = 10000

        fvectors = open(vectors_fn, "rb")
        fkeys = open(keys_fn, "r")

        ids = []
        vectors = []

        with self.id2key_db.begin(write=True) as txn_id2key:
            with self.key2id_db.begin(write=True) as txn_key2id:
                for key in tqdm.tqdm(fkeys, desc=f"reading {self.table_name} keys"):
                    vector = numpy.load(fvectors)
                    vectors.append(vector.tolist())
                    # Create engine while detecting dimension
                    if not self.engine:
                        # pass
                        dimension = len(vectors[0])
                        self._init_engine(self.table_name, dimension, self.index_type)

                    key_val = key.replace("\n", "").encode()[:510]
                    id_val = b"%d" % self.i

                    try:
                        txn_id2key.put(id_val, key_val, dupdata=False, overwrite=False)
                        txn_key2id.put(key_val, id_val, dupdata=False, overwrite=False)
                    except:
                        print(f"Failed to index in lmdb, key: {key_val}, id: {id_val}")

                    ids.append(self.i)
                    self.i += 1
                    if len(ids) >= BATCH_SIZE:
                        #                        self.engine.index(vectors, ids, partition=dataset)
                        self.engine.index(vectors, ids)
                        ids = []
                        vectors = []
                        print(f"Indexed {self.i} vectors")

                if len(ids):
                    #                    self.engine.index(vectors, ids, partition=dataset)
                    self.engine.index(vectors, ids)

        fvectors.close()
        fkeys.close()

    def _init_engine(self, table, dimension, index_type):
        print(f"Creating table: {table} with dimension {dimension}")
        self.engine = SearchEngine(
            table,
            dimension=dimension,
            index_type=INDEX_TYPE[index_type]["type"],
            index_params=INDEX_TYPE[index_type]["params"],
        )


def parse_args():
    parser = argparse.ArgumentParser(
        description="Index directory with keys and bin files to milvus"
    )
    parser.add_argument(
        "--input",
        "-i",
        help="Input directory or S3 bucket with encoded files inside other dirs (each folder should contain keys.txt and embeds_nn.bin)",
        required=True,
    )
    parser.add_argument(
        "--table",
        "-t",
        help="Table name to index records inside milvus",
        type=str,
        required=True,
    )

    parser.add_argument(
        "--index_type",
        "-it",
        help="Milvus index type",
        choices=INDEX_TYPE.keys(),
        type=str,
        required=True,
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    if args.input.startswith("s3://"):
        s3_url = args.input.replace("s3://", "").split("/")
        bucket = s3_url[0]
        prefix = "/".join(s3_url[1:])
        files_iter = S3FileRecordIterator(bucket, prefix)
    else:
        files_iter = FileRecordIterator(args.input)
    indexer = MilvusIndexer(args.table, args.index_type)
    for keys_fn, embeds_fn, dataset in files_iter():
        indexer.index(embeds_fn, keys_fn, dataset)
