import logging
import numpy as np
import os
import argparse
import tqdm
from sklearn.decomposition import PCA
import boto3
import tempfile
import shutil
import time

from collections import OrderedDict


class S3FileRecordIterator:
    def __init__(self, bucket, prefix):
        self.bucket = bucket
        self.prefix = prefix
        self.tmpdir = tempfile.gettempdir()

    def __call__(self, only_keys=False):
        self.aws_session = boto3.Session(profile_name="default")
        self.client = self.aws_session.client(
            "s3", endpoint_url="https://s3.eu-central-1.wasabisys.com"
        )
        paginator = self.client.get_paginator("list_objects_v2")
        page_iterator = paginator.paginate(Bucket=self.bucket, Prefix=self.prefix)

        datasets = OrderedDict()
        for page in page_iterator:
            for s3_record in page["Contents"]:
                _, dataset_id, file_name = s3_record["Key"].split("/")
                if not dataset_id in datasets:
                    datasets[dataset_id] = OrderedDict()
                datasets[dataset_id][file_name] = s3_record["Key"]

        for dataset_id, dataset_files in datasets.items():
            output = list()
            for file_name in ["keys.txt", "embeds_nn.bin"]:
                output.append(os.path.join(self.tmpdir, file_name))
                if not only_keys or file_name == "keys.txt":
                    logging.info(
                        f"Downloading file from S3: {dataset_files[file_name]}"
                    )
                    self.client.download_file(
                        self.bucket, dataset_files[file_name], output[-1]
                    )
            output.append(dataset_id)
            yield output


class FileRecordIterator:
    def __init__(self, path):
        self.path = path

    def __call__(self):
        for dir_w_encoded_records in sorted(os.listdir(self.path)):
            keys_fn = os.path.join(self.path, dir_w_encoded_records, "keys.txt")
            embeds_fn = os.path.join(self.path, dir_w_encoded_records, "embeds_nn.bin")
            if os.path.isfile(keys_fn) and os.path.isfile(embeds_fn):
                yield (keys_fn, embeds_fn, dir_w_encoded_records)


class FileRecordOutput:
    def __call__(self, out_dir, X_train_names, X_new_final, Ufit):
        embeddings_out = os.path.join(out_dir, "embeds_nn.bin")
        keys_out = os.path.join(out_dir, "keys.txt")

        final_pca_embeddings = {}
        logging.info(f"Writing embeddings to {embeddings_out}")
        with open(embeddings_out, "wb") as embedding_file:
            with open(keys_out, "w") as keys_file:
                for j, x in enumerate(X_train_names):
                    final_pca_embeddings[x] = X_new_final[j]
                    for u in Ufit[0:7]:
                        final_pca_embeddings[x] = (
                            final_pca_embeddings[x]
                            - np.dot(u.transpose(), final_pca_embeddings[x]) * u
                        )
                    np.save(embedding_file, final_pca_embeddings[x])
                logging.info(f"Wrote {j+1} vectors")
                logging.info(f"Writing keys to {keys_out}")
                keys_file.writelines(X_train_names)
                # for t in final_pca_embeddings[x]:
                #    embedding_file.write("%f\t" % t)


class S3FileRecordOutput(FileRecordOutput):
    def __init__(self, bucket, prefix):
        self.bucket = bucket
        self.prefix = prefix
        self.client = boto3.client(
            "s3", endpoint_url="https://s3.eu-central-1.wasabisys.com"
        )

    def __call__(self, out_dir, X_train_names, X_new_final, Ufit):
        dataset_id = out_dir.split("/")[-1]
        super().__call__(out_dir, X_train_names, X_new_final, Ufit)
        for f in ["embeds_nn.bin", "keys.txt"]:
            src_f = os.path.join(out_dir, f)
            tgt_f = os.path.join(self.prefix, dataset_id, f)
            logging.info(f"Uploading {src_f} to {tgt_f}")
            self.client.upload_file(src_f, self.bucket, tgt_f)
        shutil.rmtree(out_dir)


class ReduceDimension:
    BATCH = 50000
    #    BATCH = 1000

    def __init__(self, dim, outputer):
        self.dim = dim
        self.X_train = []
        self.X_train_names = []
        self.src_dim = None
        self.i = 0
        self.outputer = outputer

    def reduce(self, vectors_fn, keys_fn, out_dir, skip_batch=False):

        if not vectors_fn:
            batch_out_dir = os.path.join(out_dir, str(self.i))
            os.makedirs(batch_out_dir, exist_ok=True)
            self._reduce_batch(batch_out_dir)
            self.X_train = []
            self.X_train_names = []
            return

        fvectors = open(vectors_fn, "rb")
        fkeys = open(keys_fn, "r")

        for key in tqdm.tqdm(fkeys, desc=f"reading {fvectors} keys"):
            if not skip_batch:
                vector = np.load(fvectors)
                self.X_train.append(vector.tolist())
                if self.src_dim is None:
                    self.src_dim = len(self.X_train[0])
                    logging.info(f"Detected source dimension: {self.src_dim}")
            self.X_train_names.append(key)

            if len(self.X_train_names) >= self.BATCH:
                batch_out_dir = os.path.join(out_dir, str(self.i))
                os.makedirs(batch_out_dir, exist_ok=True)

                logging.info(f"Accumulated size: {len(self.X_train_names)}")
                if not skip_batch:
                    logging.info(f"Reducing batch {self.i}")
                    self._reduce_batch(batch_out_dir)
                else:
                    logging.info(f"Skipping batch {self.i}")
                    self.i += 1

                # Clear
                self.X_train = []
                self.X_train_names = []

    def _reduce_batch(self, out_dir):
        logging.info(f"Reducing batch {self.i}")
        X_train = self.X_train  # np.asarray(self.X_train)
        X_train_names = self.X_train_names
        pca_embeddings = {}

        # PCA to get Top Components
        logging.info("PCA 1")
        pca = PCA(n_components=self.src_dim)
        X_train = X_train - np.mean(X_train)
        X_fit = pca.fit_transform(X_train)
        U1 = pca.components_

        z = []

        # Removing Projections on Top Components
        for i, x in enumerate(X_train):
            for u in U1[0:7]:
                x = x - np.dot(u.transpose(), x) * u
            z.append(x)

        z = np.asarray(z)

        # PCA Dim Reduction
        logging.info("PCA 2")
        pca = PCA(n_components=self.dim)
        X_train = z - np.mean(z)
        X_new_final = pca.fit_transform(X_train)

        # PCA to do Post-Processing Again
        logging.info("PCA 3")
        pca = PCA(n_components=self.dim)
        X_new = X_new_final - np.mean(X_new_final)
        X_new = pca.fit_transform(X_new)
        Ufit = pca.components_

        X_new_final = X_new_final - np.mean(X_new_final)

        # Delegate generation output to outputer object
        self.outputer(out_dir, X_train_names, X_new_final, Ufit)

        self.i += 1


def parse_args():
    parser = argparse.ArgumentParser(
        description="Index directory with keys and bin files to milvus"
    )
    parser.add_argument(
        "--input",
        "-i",
        help="Input directory with encoded files inside other dirs (each folder should contain keys.txt and embeds_nn.bin)",
        required=True,
    )
    parser.add_argument(
        "--output",
        "-o",
        help="Output directory where reduced embeddings will be written",
        required=True,
    )
    parser.add_argument(
        "--dimension", "-d", help="Target dimension", type=int, default=128
    )
    parser.add_argument("--skip-to-batch", "-sb", help="Skip to this batch", type=int)

    return parser.parse_args()


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s:%(levelname)s:%(message)s", level=logging.INFO
    )
    args = parse_args()
    # files_iter = FileRecordIterator(args.input)
    files_iter = S3FileRecordIterator("jhn01", "encoded_records_st")
    reductor = ReduceDimension(
        args.dimension, S3FileRecordOutput("jhn01", "encoded_records_st_128")
    )
    skip_batch = args.skip_to_batch != None
    for keys_fn, embeds_fn, dataset in files_iter(only_keys=skip_batch):
        reductor.reduce(embeds_fn, keys_fn, args.output, skip_batch)
        skip_batch = args.skip_to_batch != None and reductor.i < args.skip_to_batch
    reductor.reduce(None, None, args.output)  # finalize last batch

