FROM python:3.6.9

RUN mkdir -p /opt/recsy
COPY requirements.txt /opt/recsy/requirements.txt
WORKDIR /opt/recsy
RUN pip install -r requirements.txt
COPY src /opt/recsy/src
RUN python3 -m nltk.downloader stopwords
RUN mv /root/nltk_data /usr/share/nltk_data


CMD cd /opt/recsy/src && python3 recommenders/europeana/main.py --port 5090
